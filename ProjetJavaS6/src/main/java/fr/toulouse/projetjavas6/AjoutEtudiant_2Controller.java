/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.GestionCSV;
import fr.toulouse.projetjavas6.Backend.Mention;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML AjoutEtudiant_2
 */
public class AjoutEtudiant_2Controller implements Initializable{
    
    
    private Formation form = new Formation();
    private Mention m=new Mention();
    
    
    
    @FXML
    private Button btValider;
    
    
    @FXML
    private AnchorPane AnchorPaneAj3;
    @FXML
    private ComboBox cbAnneeDiplome;
    @FXML
    private ComboBox cbMention;
    @FXML
    private ComboBox cbParcours;
    
    
    
    //permet de remplir la combobox
    //remplacer les noms de filières avec fichier csv
    ObservableList<String> anneesList = FXCollections.observableArrayList("----------LICENCE----------","L1","L2","L3","-----MASTER-----","M1","M2", "-----BUT-----","Annee1","Annee2"); 
    ObservableList<String> mentionList = FXCollections.observableArrayList("----------LICENCE----------","Informatique","Miashs","EEA","Genie Civil","Mecanique","Science de la Terre","Science de la Vie","-----MASTER-----","Informatique","MIAGE","Science de la Terre et des planetes environnement","Biologie vegetale","EEA","Genie civil","Chimie","Mathematiques et applications", "STAPS","----------BUT-----------","Chimie","Genie Biologique","Genie chimique");
    ObservableList<String> parcourslist =FXCollections.observableArrayList("----------LICENCE----------","DIFS","MIDL","INFO","IRT","MIAGE","IO","EEA-EAD","EAF","ISS", "------------MASTER------------","DC-IA","IDP","ITN","SGE");//à completer
    
    GestionCSV csv = new GestionCSV();
    AjoutEtudiantController aj = new AjoutEtudiantController();
   
    
    @Override
    /**
     * affiche les combobox pour l'inscription d'un etudiant dans une filiere 
     * @param URL url
     * @param ResourceBundle rb
     * 
     */
    public void initialize(URL url, ResourceBundle rb) {
        this.cbAnneeDiplome.setValue("Choisir une année");
        this.cbAnneeDiplome.setItems(anneesList);
       
        this.cbMention.setValue("Choisir une mention");
        this.cbMention.setItems(mentionList);
        
        this.cbParcours.setValue("Choisir un parcours");
        this.cbParcours.setItems(parcourslist);
    }
    
    @FXML
    /**
     * ajoute l'etudiant dans la formation remplie
     *
     */
    public void handleSuivant(){
        try {
    
            
            int longueur = App.ListeEtudiant.size();
            setNiveauEtudiant(App.ListeEtudiant.get(longueur-1));
            Etudiant etu = App.ListeEtudiant.get(longueur-1);
                
            
            
            if(ajoutEtudiantFormation(etu)){


                csv.ecritureCSVetudiant("donnee_application/ListeEtudiant.csv", App.ListeEtudiant);
                csv.ecritureCSVformation("donnee_application/Formation.csv", App.ListeFormation);






                FXMLLoader loader = new FXMLLoader(getClass().getResource("Accueil.fxml"));
                Parent root = loader.load();

                  Stage stage = (Stage) btValider.getScene().getWindow();
                stage.setScene(new Scene(root,1290, 750));
            }
        } catch (IOException ex) {
        }
    
        
    }
    /**
     * ajoute le numero d'un etudiant à la liste de formation pour la formation selectionnée
     * @param  e etudiant à ajouter dans une formation
     * @return boolean true si la formation selectionnée existe sinon false
     */
    public boolean ajoutEtudiantFormation(Etudiant e){
        int i=0;
        boolean b = false;
        this.form.setAnneeDiplome(cbAnneeDiplome.getValue()+"");
        this.m.setNomMention(cbMention.getValue()+"");
        this.m.setParcours(cbParcours.getValue()+"");
        this.form.setMention(m);
        
        
        
        
     
        while(i<App.ListeFormation.size()-1 &&(  !App.ListeFormation.get(i).getAnneeDiplome().equals(this.form.getAnneeDiplome()) || !App.ListeFormation.get(i).getMention().getNomMention().equals(form.getMention().getNomMention()) || !App.ListeFormation.get(i).getMention().getParcours().equals(form.getMention().getParcours()))){
            i++;
        }
        
        if (i==App.ListeFormation.size()-1){
            Alert alertFormation = new Alert(Alert.AlertType.ERROR);
            alertFormation.setTitle("Formation non existante !");
            alertFormation.setHeaderText("Veuillez inserer une formation valide");
            alertFormation.showAndWait();
        }
        else{
            String listeEtudiantInscrits = App.ListeFormation.get(i).getListeNumEtudiant();
            
            if ("null".equals(App.ListeFormation.get(i).getListeNumEtudiant())){
                App.ListeFormation.get(i).setListeNumEtudiant(e.getNumeroEtudiant()+"/");
            }
            else{
              App.ListeFormation.get(i).setListeNumEtudiant(listeEtudiantInscrits + e.getNumeroEtudiant()+"/");
            }
            
            
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Etudiant Ajouté !");
            alert.setHeaderText("Etudiant ajouté dans sa formation");
            alert.showAndWait();
            b=validerInscription(b);
        }
        return b;
        
  
       
    }
    
    /**
     * 
     * @param b
     * @return true
     */   
    public boolean validerInscription(boolean b){
        return b = true;
    }


            
        
        
        
    
    
    
    /**
     * modifie le niveau de l'étudiant en fonction du niveau choisi dans la combobox
     * @param etu 
     */
    public void setNiveauEtudiant(Etudiant etu){
        
        switch (cbAnneeDiplome.getValue()+"") {
            case "L1":
                etu.setNiveau(1);
                break;
            case "L2":
                etu.setNiveau(2);
                break;
            case "L3":
                etu.setNiveau(3);
                break;
            case "M1":
                etu.setNiveau(4);
                break;
            case "M2":
                etu.setNiveau(5);
                break;
        }
                
        
    }
}

