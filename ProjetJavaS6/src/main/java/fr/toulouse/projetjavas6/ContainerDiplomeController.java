package fr.toulouse.projetjavas6;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */


import fr.toulouse.projetjavas6.Backend.Formation;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * Controller du fichier FXML ContainerDiplome
 */
public class ContainerDiplomeController{


    @FXML
    private Label lbNomMention;
    @FXML
    private Label lbNomParcours;
    
    private ListenerAccueil listener;
    
    private List<Formation> listeFormation;
    
    private VBox vbox;
    
    /**
     * gère le click sur un nom de Mention
     * @param mouseEvent 
     */
    @FXML
    private void click(MouseEvent mouseEvent) {
        listener.onClickListener(listeFormation, this.vbox);
    }   
    
    /**
     * Setter du nom de la mention
     * @param nomMention
     * nom de la mention courante
     */
    public void setNomMention(String nomMention){
        lbNomMention.setText(nomMention);
    }
    
    /**
     * Setter du nom du parcours
     * @param nomParcours
     * nom du parcours courant
     */
    public void setNomParcours(String nomParcours){
        lbNomParcours.setText(nomParcours);
    }
    
    /**
     * Initialisation du container avec ke nom de la mention et du parcours
     * @param listeFormation
     * liste de la formation courante
     * @param listener
     * Permet de mettre a jour le Listener
     */
    public void intialiserContainerDiplome(List<Formation> listeFormation, ListenerAccueil listener){
        
        List<Formation> updateListeFormation = new ArrayList<>();
        
        for (Formation f : listeFormation) {
            if (f.getMention().getNomMention().equals(lbNomMention.getText()) && f.getMention().getParcours().equals(lbNomParcours.getText())) {
                updateListeFormation.add(f);
            }
        }
        
        this.listeFormation=updateListeFormation;
        this.listener=listener;
        
    }
    
    

    /**
     * parmet de passer l'element FXML de la page d'accueil au fichier FXML suivant
     * @param vbox
     * element FXMM parent général de la page accueil
     */
    public void passerElem(VBox vbox){
        this.vbox = vbox;
    }
    
   
}
