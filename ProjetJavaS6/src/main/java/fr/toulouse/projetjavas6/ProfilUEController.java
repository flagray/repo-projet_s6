/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6;

import static fr.toulouse.projetjavas6.App.ListeEtudiant;
import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.UniteEnseignement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.event.ActionEvent;

/**
 * Controller du fichier FXML ProfilUE
 */
public class ProfilUEController{

    @FXML
    private AnchorPane backAnchorPane;
    @FXML
    private Circle cPhoto;
    @FXML
    private Label lbCodeUE;
    @FXML
    private Label lbNomUE;
    @FXML
    private Label lbECT;
    @FXML
    private Button btModifEtudiant;
    @FXML
    private Label lbNomFormation;
    @FXML
    private Button btAjoutEtudiant;
    @FXML
    private Label lbAjoutEtudiants;
    @FXML
    private Text btAjoutEtudiant1;
    @FXML
    private Text btAjoutEtudiant11;
    @FXML
    private Label lbNbEtudiant;
    @FXML
    private GridPane listEtudiantGrid;
    @FXML
    private Button btUEValider;
    
    private Listener listener;
   
    private Formation formation;
    
    private UniteEnseignement UE;
    
    
    
    /**
     * Permet d'initialiser la page profil UE avec le remplissage des labels, la gestion des boutons ajout etudiant et validation UE
     * @param UE
     * UE courante
     * @param formation
     * Formation courante
     */
    public void initialiserProfilUE(UniteEnseignement UE, Formation formation){
       
        lbCodeUE.setText(UE.getCodeUE());
        lbNomUE.setText(UE.getNomUE());
        String nbECTS = String.valueOf(UE.getNbECTS());
        lbECT.setText(nbECTS);
        lbNomFormation.setText(formation.getMention().getNomMention());
        btAjoutEtudiant.setOnAction(event -> {
                                                try {
                                                    FXMLLoader loader = new FXMLLoader(getClass().getResource("InscriptionEtudiantUE.fxml"));
                                                    Parent root = loader.load();
                                                    
                                                    InscriptionEtudiantUEController inscriptionEtudiantUEController = loader.getController();
                                                    inscriptionEtudiantUEController.inscriptionEtudiant(UE, formation);
                                                    
                                                    Stage stage = (Stage) backAnchorPane.getScene().getWindow();
                                                    Scene scene = stage.getScene();
                                                    
                                                    inscriptionEtudiantUEController.affichageButtonRetour(stage, scene);

                                                    stage.setScene(new Scene(root,1290, 750));

                                                } catch (IOException ex) {
                                                ex.printStackTrace();
                                                }
                                             });
        btUEValider.setOnAction(event -> {
                                            try {
                                                FXMLLoader loader = new FXMLLoader(getClass().getResource("ValidationEtudiantUE.fxml"));
                                                Parent root = loader.load();
                                                
                                                ValidationEtudiantUEController validationEtudiantUEController = loader.getController();
                                                
                                                Stage stage = (Stage) backAnchorPane.getScene().getWindow();
                                                Scene scene = stage.getScene();
                                                
                                                validationEtudiantUEController.affichageButtonRetour(stage, scene);
                                                validationEtudiantUEController.validerEtudiantUE(UE, formation);
                                               
                                                stage.setScene(new Scene(root,1290, 750));
                                            } catch (IOException ex) {
                                                ex.printStackTrace();
                                            }
                                            
                                         });
    }
    

    /**
     * permet de gérer l'affichage dynamique des card etudiant
     * @param UE
     * UE courante
     */
    public void affichageCardEtudiant(UniteEnseignement UE){
        this.UE = UE;
        
        List<Etudiant> updateListeEtudiant = new ArrayList<>();
        
        String[] words = UE.getListeNumEtudiant().split("/");
       
        for(int i=0; i<App.ListeEtudiant.size();i++){
            String numeroEtudiant = String.valueOf(App.ListeEtudiant.get(i).getNumeroEtudiant());
            for (int j=0; j<words.length; j++) {
                if (words[j].equals(numeroEtudiant)) {
                    updateListeEtudiant.add(App.ListeEtudiant.get(i));
                }
            }
        }
        
        
        String nbEtudiant = String.valueOf(updateListeEtudiant.size());
        lbNbEtudiant.setText(nbEtudiant);
        
        
        //si la liste d'étudiant n'est pas vide
        if(updateListeEtudiant.size() > 0){
            
            //on fait appel au listener et on définit l'action à réaliser après un click 
            //(definition ici de la méthode dans l'interface)
            listener = new Listener() {
                @Override
                public void onClickListener(Etudiant etudiant, Formation formation, AnchorPane anchorPane) {
                    
                    try{
                        
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("ProfilEtudiant.fxml"));
                        Parent root = loader.load();
                        ProfilEtudiantController profilEtudiantController = loader.getController();
                        
                        profilEtudiantController.initialiserProfil(etudiant);
                        profilEtudiantController.creationTabAnnee(etudiant.getNiveau(), formation);
                        profilEtudiantController.setProgressBarAnnee(formation);
                        
                        
                        
                        if (anchorPane != null) {
                            Stage stage = (Stage) anchorPane.getScene().getWindow();
                            Scene scene = stage.getScene();
                            
                            profilEtudiantController.affichageButtonRetour(stage, scene);//on envoie la scene courant au bouton retour pour revenir dessus quand on clic
                            
                            stage.setScene(new Scene(root,1290, 750));
                            stage.show(); 
                        }

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            };
        }
        
        int column = 0;
        int row = 1;
        
        try {
            for(int i=0; i<updateListeEtudiant.size();i++){
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("CardEtudiant.fxml"));
                
                AnchorPane cardEtudiant = fxmlLoader.load();
                CardEtudiantController cardEtudiantController = fxmlLoader.getController();
                cardEtudiantController.setDataCardEtudiant(updateListeEtudiant.get(i), listener);
                cardEtudiantController.sendData(this.backAnchorPane, this.formation);
                
                if(column == 5){
                    column = 0;
                    row++;
                }
                
                listEtudiantGrid.add(cardEtudiant, column++, row);
                GridPane.setMargin(cardEtudiant, new Insets(10));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Permet d'avoir accès à la formation courante envoyée par la page précédente
     * @param formation
     * formation courante
     */
    public void sendFormation (Formation formation){
        this.formation = formation;
    }

    
    /**
     * Permet de trier par prénom les UE affichées
     * @param event 
     */
    @FXML
    private void trierParPrenom(ActionEvent event) {
        
        Collections.sort(ListeEtudiant, new Comparator<Etudiant>(){
        @Override
        public int compare(Etudiant e1, Etudiant e2){
            return e1.getPrenom().compareTo(e2.getPrenom());
        }
        });
        this.affichageCardEtudiant(this.UE);
            
 
        
    }

    /**
     * Permet de trier par nom les UE affichées
     * @param event 
     */
    @FXML
    private void trierParNom(ActionEvent event) {
        Collections.sort(ListeEtudiant, new Comparator<Etudiant>(){
        @Override
        public int compare(Etudiant e1, Etudiant e2){
            return e1.getNom().compareTo(e2.getNom());
        }
        });
        this.affichageCardEtudiant(this.UE);
    }

    
    
}
