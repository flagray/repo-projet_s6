/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Formation;
import java.io.IOException;
import java.util.List;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML VueGeneraleTabPane
 */
public class VueGeneraleTabPaneController{

    @FXML
    private TabPane tbDiplome;
    private TabPane sousTabpane;
    
    @FXML
    private AnchorPane backAnchorPane;
    
    
    private List<Formation> listeFormation;
    
    
    
    /**
     * Initialisation de la page avec l'element FXML tabPane
     * @param listeFormation
     * Liste de formation courante
     */
    public void initialisationVueGenerale(List<Formation> listeFormation){
        
        //Gestion du bouton retour
        Button backButton = new Button("Retour");
        backAnchorPane.getChildren().add(backButton);
        
        //Style du bouton retour
        backButton.setStyle("-fx-background-color: none; -fx-font-size:28.0;");
        backButton.setText("↩");
        backButton.setLayoutX(12.0);
        backButton.setLayoutY(60.0);

        backButton.setOnAction(event -> {
                            try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("Accueil.fxml"));
                            Parent root = loader.load();
                            
                            Stage stage = (Stage) backButton.getScene().getWindow();
                            stage.setScene(new Scene(root,1290, 750));
                            
                            } catch (IOException ex) {
                            ex.printStackTrace();
                            }
        });

        //Gestion création TabPane vide
        for(int i=0; i<listeFormation.size(); i++) {
            
            Tab tabAnneeDiplome = new Tab(listeFormation.get(i).getAnneeDiplome());
            this.tbDiplome.getTabs().add(tabAnneeDiplome);
            
            AnchorPane ap = new AnchorPane();
            this.tbDiplome.getTabs().get(i).setContent(ap);
            
            //Création sous tabPane
            Tab tabEtudiant = new Tab("ETUDIANT");
            Tab tabUE = new Tab("UNITE D'ENSEIGNEMENT");
            this.sousTabpane = new TabPane(tabEtudiant,tabUE);
            
            this.tbDiplome.getTabs().get(i).setContent(this.sousTabpane);
            this.sousTabpane.getTabs().get(0).setContent(ap);
            
            //gestion style tabpane
            this.tbDiplome.getTabs().get(i).setStyle("-fx-background-color: rgba(0,0,0,0); -fx-pref-width:501");
            this.sousTabpane.getTabs().get(0).setStyle("-fx-background-color: rgba(0,0,0,0); -fx-pref-width:700");
            this.sousTabpane.getTabs().get(1).setStyle("-fx-background-color: rgba(0,0,0,0); -fx-pref-width:700");
            
            this.remplissageTabPaneCoteEtudiant(i);
            this.remplissageTabPaneCoteUE(i);
        }
    }
    
    /**
     * Permet le remplissage du tabPane global du côté etudiant
     * @param i
     * indice de la formation courante
     */
    public void remplissageTabPaneCoteEtudiant(int i){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("ContentVueGenerale.fxml"));
            
            AnchorPane anch1 = fxmlLoader.load();
            
            ContentVueGeneraleController contentVueGeneraleController = fxmlLoader.getController();
            contentVueGeneraleController.setNomFormation(this.listeFormation.get(i).getMention().getNomMention());
            contentVueGeneraleController.affichageCardEtudiant(this.listeFormation.get(i));
            this.sousTabpane.getTabs().get(0).setContent(anch1);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Permet le remplissage du tabPane global du côté UE
     * @param i
     * indice de la formation courante
     */
    public void remplissageTabPaneCoteUE(int i){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("ContentVueGenerale.fxml"));
            
            AnchorPane anch1 = fxmlLoader.load();
            
            ContentVueGeneraleController contentVueGeneraleController = fxmlLoader.getController();
            contentVueGeneraleController.setNomFormation(this.listeFormation.get(i).getMention().getNomMention());
            contentVueGeneraleController.affichageCardUE(this.listeFormation.get(i));
         
            this.sousTabpane.getTabs().get(1).setContent(anch1);
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
  
    /**
     * Permet de mettre à jour la formation courante de la page précédente
     * @param listeFormation
     * Formation courante
     */
    public void sendData(List<Formation> listeFormation){
        this.listeFormation = listeFormation;
    }
}
