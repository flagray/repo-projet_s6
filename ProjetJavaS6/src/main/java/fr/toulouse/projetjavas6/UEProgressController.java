/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.GestionDate;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.text.Text;

/**
 * Controller du fichier FXML UEProgress
 */
public class UEProgressController {

    @FXML
    private Label lbNomUE;
    @FXML
    private ProgressBar pgUE;
    @FXML
    private CheckBox cbUE;
    @FXML
    private Text textTrue;
    @FXML
    private Text textFalse;
    
    /**
     * Permet de mettre à jour les barres de progressions des UE dans le profil etudiant
     * @param i
     * Indice de l'UE courante
     * @param formation
     * Formation courante
     * @param numeroEtudiantCourant
     * numero Etudiant courant
     */
    public void initialiserProgressBarUE(int i, Formation formation, String numeroEtudiantCourant){
        GestionDate gestionDate = new GestionDate();
        
        float resultat=0;
        
        if (App.ListeUE.get(i).getSemestre().equals("1")) {
            resultat = gestionDate.avancementProgressBar(formation.getSemestreImpair().getDateDebut(), formation.getSemestreImpair().getDateFin());
            pgUE.setProgress(resultat); 
        }
        
        else{
            resultat = gestionDate.avancementProgressBar(formation.getSemestrePair().getDateDebut(), formation.getSemestrePair().getDateFin());
            pgUE.setProgress(resultat);
        }
       
        String listeNumEtudiantValide = App.ListeUE.get(i).getListeNumEtudiantValide();
        String[] words = listeNumEtudiantValide.split("/");
        
        for (int j=0; j<words.length; j++) {
            if (words[j].equals(numeroEtudiantCourant)) {
                pgUE.setStyle("-fx-accent:#A3BFA3");
            }
        }
        
        
        if(resultat>=1 && pgUE.getStyle()=="-fx-accent:#A3BFA3"){
            textTrue.setStyle("visibility:visible");
        }
        else if (resultat>=1){
            textFalse.setStyle("visibility:visible");
            pgUE.setStyle("-fx-accent:#D77E78");
        }
        lbNomUE.setText(App.ListeUE.get(i).getNomUE());
    }
    
}
