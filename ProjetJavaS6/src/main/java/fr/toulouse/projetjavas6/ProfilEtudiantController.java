/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.GestionDate;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML ProfilEtudiantController
 */
public class ProfilEtudiantController{


    @FXML
    private Label lbPrenom;
    @FXML
    private Circle cPhoto;
    @FXML
    private Label lbNom;
    @FXML
    private Label lbNumEtudiant;
    @FXML
    private Label lbMail;
    @FXML
    private Label lbMaFormation;
    @FXML
    private ProgressBar pbFormation;
    @FXML
    private Label lbECT;
    @FXML
    private Button btModifEtudiant;
    @FXML
    private TabPane tabPaneAnnee;
    @FXML
    private AnchorPane backAnchorPane;
    
    
    private Formation formationCourante;
    
    private Etudiant etu;
    
    
    
    /**
     * Gère l'initialisation des données pour la page profil étudiant
     * @param etudiant
     * Etudiant courant
     */
    public void initialiserProfil(Etudiant etudiant){
    
        this.pbFormation.setProgress(0.5);
        
        this.lbPrenom.setText(etudiant.getPrenom());
        this.lbNom.setText(etudiant.getNom());
        Integer numEtu = etudiant.getNumeroEtudiant();
        this.lbNumEtudiant.setText(numEtu.toString());
        this.lbMail.setText(etudiant.getMail());
        Integer nbECT = etudiant.getNbECTS();
        this.lbECT.setText(nbECT.toString());
        this.etu=etudiant;
        
    } 
    
    /**
     * Met à jour l'affichage de la barre de progression pour l'année en cours
     * @param formation
     * formation courante
     */
    public void setProgressBarAnnee(Formation formation){
        GestionDate gestionDate = new GestionDate();
        
        float resultat = gestionDate.avancementProgressBar(formation.getAnnee().getDateDebut(), formation.getAnnee().getDateFin());
        
        this.pbFormation.setProgress(resultat);
    }
    
    /**
     * Création des tab pour afficher les différentes années d'études d'un etudiant
     * @param nbAnnee
     * Permet de connaitre le nombre d'année d'etude de l'etudiant
     * @param formation
     * formation courante
     */
    public void creationTabAnnee(int nbAnnee, Formation formation){ 
    
        for(int i=0; i<nbAnnee; i++) {
            Tab tab1 = new Tab("Année "+ (i+1));
            
            //Style des onglets tab
            tab1.setStyle("-fx-background-color:#A3B7BF; -fx-background-radius: 10 10 0 0; -fx-font-family:Calibri Light; -fx-font-size:18;");
            
            tabPaneAnnee.getTabs().add(tab1);
            tabPaneAnnee.getSelectionModel().select(i);
        }
        
        this.remplissageTabAnnee(nbAnnee, formation);
        this.formationCourante=formation;
    }
    
    /**
     * Permet de remplir les tab des différentes années d'etudes d'un etudiant
     * @param numeroAnnee
     * Permet l'affichage du numéro de l'année pour chaque tab
     * @param formation
     * formation courante
     */
    public void remplissageTabAnnee(int numeroAnnee, Formation formation){
        numeroAnnee = numeroAnnee-1;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("ContentTab.fxml"));
            
            AnchorPane anch1 = fxmlLoader.load();
            
            ContentTabController contentTabController = fxmlLoader.getController();
            contentTabController.recuperationListeUEEtudiant(lbNumEtudiant.getText().toString());
            contentTabController.affichageSemestre(1, formation);
            contentTabController.affichageSemestre(2, formation);
            contentTabController.setNomFormation(formation.getMention().getNomMention());
            
            tabPaneAnnee.getTabs().get(numeroAnnee).setContent(anch1);
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     *
     * Permet de retourner à la page précédente
     * @param stage
     * Stage de la sccene précédente
     * @param scene
     * Scene de la scene précédente
     */
    public void affichageButtonRetour(Stage stage, Scene scene){
        //Gestion du bouton retour
        Button backButton = new Button("Retour");
        backAnchorPane.getChildren().add(backButton);
        
        //Style du bouton retour
        backButton.setStyle("-fx-background-color:none; -fx-font-size:28.0");
        backButton.setText("↩");
        backButton.setLayoutX(12.0);
        backButton.setLayoutY(60.0);

        backButton.setOnAction(event -> {
                            stage.setScene(scene);
                            });
    }
    
    
    /**
     * Gère le clic sur le bouton de modification d'un étudiant
     */
    @FXML
    /**
     * changement de page vers la page de modification de l'etudiant
     */
    private void modifierEtudiant(){
        backAnchorPane.getChildren().add(btModifEtudiant);
        try {
            
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ModifierEtudiant.fxml"));
            Parent root = loader.load();
            
            ModifierEtudiantController modifierEtudiantController = loader.getController();
            modifierEtudiantController.recupEtudiant(etu);
            modifierEtudiantController.recupererFormation(formationCourante);
            Stage stage = (Stage) backAnchorPane.getScene().getWindow();
            stage.setScene(new Scene(root,1290, 750));
            
            
            
            
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    
    /**
     * permet de récuperer le numéro etudiant courant depuis l'interface
     * @return label
     * retourne le numéro etudiant courant sous forme de label
     */
    public Label getNumEtudiant(){
        return this.lbNumEtudiant;
    }
    
    
    

}
