/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.GestionCSV;
import fr.toulouse.projetjavas6.Backend.UniteEnseignement;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML ValidationEtudiantUE
 */
public class ValidationEtudiantUEController{


    @FXML
    private VBox backVbox;
    @FXML
    private ListView<CheckBox> ListViewEtudiant;
    @FXML
    private Button button;
   
    
    
    /**
     * Permet d'afficher la liste de profil étudiant à valider et d'effectuer la validation et de stocker la modification dans le fichier CSV
     * @param UE
     * UE Courante
     * @param formation
     * Formation courante
     */
    public void validerEtudiantUE(UniteEnseignement UE, Formation formation){
        
        List<Etudiant> listeEtudiantInscrit = new ArrayList<>();
        List<Etudiant> listeEtudiantValide = new ArrayList<>();
        
        //recuperation de la liste d'étudiant inscrit à l'ue
        String[] numeroEtudiantUE = UE.getListeNumEtudiant().split("/");
       
        for(int i=0; i<App.ListeEtudiant.size();i++){
            String numeroEtudiant = String.valueOf(App.ListeEtudiant.get(i).getNumeroEtudiant());
            for (int j=0; j<numeroEtudiantUE.length; j++) {
                if (numeroEtudiantUE[j].equals(numeroEtudiant)) {
                    listeEtudiantInscrit.add(App.ListeEtudiant.get(i));
                }
            }
        }
        
        //recuperation de la liste d'étudiant qui ont validé l'UE
        String[] numeroEtudiantValide = UE.getListeNumEtudiantValide().split("/");
       
        for(int i=0; i<App.ListeEtudiant.size();i++){
            String numeroEtudiant = String.valueOf(App.ListeEtudiant.get(i).getNumeroEtudiant());
            for (int j=0; j<numeroEtudiantValide.length; j++) {
                if (numeroEtudiantValide[j].equals(numeroEtudiant)) {
                    listeEtudiantValide.add(App.ListeEtudiant.get(i));
                }
            }
        }
        
        
        List<Etudiant> listeEtudiantAvalider = new ArrayList<>();
        listeEtudiantAvalider.addAll(listeEtudiantInscrit);
        
        for(int i=0; i<listeEtudiantInscrit.size(); i++){
            for(int j=0; j<listeEtudiantValide.size(); j++){
                if(listeEtudiantInscrit.get(i).getNumeroEtudiant() == listeEtudiantValide.get(j).getNumeroEtudiant()){
                    listeEtudiantAvalider.remove(listeEtudiantValide.get(j));
                }
            }
        }
        
        //Style ListView
        ListViewEtudiant.setStyle("-fx-font-family:Calibri Light; -fx-control-inner-background: #F5F5F5; -fx-control-inner-background-alt: derive(-fx-control-inner-background, 50%);");
        
        for (int i = 0; i < listeEtudiantAvalider.size(); i++) {
            String numEtudiant = String.valueOf(listeEtudiantAvalider.get(i).getNumeroEtudiant());
            CheckBox check = new CheckBox(numEtudiant);
            ListViewEtudiant.getItems().add(check);
        }
        
    
        button.setOnAction(event -> {
                                    String nouvelleListe ="";
                                    for (CheckBox box : ListViewEtudiant.getItems()) {
                                        if (box.isSelected()) {
                                            nouvelleListe+= box.getText()+"/";
                                        }
                                    }
            
                                    for(int i=0; i<App.ListeUE.size();i++){
                                        if (App.ListeUE.get(i).getCodeUE().equals(UE.getCodeUE())) {
                                            String listeEtudiantValideExistante = App.ListeUE.get(i).getListeNumEtudiantValide();
                                            if(listeEtudiantValideExistante.equals("null")){
                                                App.ListeUE.get(i).setListeNumEtudiantValide(nouvelleListe);
                                            }
                                            else{
                                                App.ListeUE.get(i).setListeNumEtudiantValide(listeEtudiantValideExistante+nouvelleListe);
                                            }
                                            
                                        }
                                    }
                                    
                                    GestionCSV g1 = new GestionCSV();
                                    
                                    g1.ecritureCSVUE("donnee_application/UE.csv", App.ListeUE);
                                    
                                    //System.out.println(App.ListeUE.get(0).getListeNumEtudiant());
                                    Stage stage = (Stage) button.getScene().getWindow();
                                    stage.close();
                                    });
  
    }

    /**
     *
     * Permet de retourner à la page précédente
     * @param stage
     * Stage de la sccene précédente
     * @param scene
     * Scene de la scene précédente
     */
    public void affichageButtonRetour(Stage stage, Scene scene){
        //Gestion du bouton retour
        Button backButton = new Button("Retour");
        backVbox.getChildren().add(backButton);

        //Style du bouton de retour
        backButton.setStyle("-fx-background-color:none; -fx-font-size:28.0");
        backButton.setText("↩");
        backButton.setLayoutX(12.0);
        backButton.setLayoutY(60.0);
        
        backButton.setOnAction(event -> {
                            stage.setScene(scene);
                            });
    }
    
}