/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.UniteEnseignement;

/**
 * Ecouteur pour savoir si une UE est cliqué par l'utilisateur
 */
public interface ListenerUE {

    /**
     * permet de gérer le clic sur les UE
     * @param UE
     * UE cliqué par l'utilisateur
     * @param formation
     * Formation contenant les UE
     */
    
    public void onClickListener(UniteEnseignement UE, Formation formation);
}
