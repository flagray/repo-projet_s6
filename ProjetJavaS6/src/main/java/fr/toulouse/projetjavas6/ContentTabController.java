/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.GestionDate;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * Controller du fichier FXML ContentTab
 */
public class ContentTabController{


    @FXML
    private Label lbNomFromation;
    @FXML
    private ProgressBar pbSemestre1;
    @FXML
    private ProgressBar pbSemestre2;
    @FXML
    private Button btFlecheSem1;
    @FXML
    private Button btFlecheSem2;
    @FXML
    private GridPane listUEGridS1;
    @FXML
    private GridPane listUEGridS2;
    
    private List<Integer> listeUEIndex;
    
    private String numEtudiantCourant;
   
    
    /**
     * Setter du nom de la formation
     * @param nomFormation
     * nom de la formation courante
     */
    public void setNomFormation(String nomFormation){
        lbNomFromation.setText(nomFormation);
    }
    
    /**
     * Affichage des 2 semestres d'un étudiant lors du clic sur son profil, avec mise à jours des barres de progressions et gestion de l'affichage des UE dans chaque semestre
     * @param numeroSemestre
     * semestre pair ou impair
     * @param formation
     * nom de la formation courante
     */
    public void affichageSemestre(int numeroSemestre, Formation formation){
        
        GestionDate gestionDate = new GestionDate();
        
        if (numeroSemestre==1){
            float resultat = gestionDate.avancementProgressBar(formation.getSemestreImpair().getDateDebut(), formation.getSemestreImpair().getDateFin());
            pbSemestre1.setProgress(resultat); 
        }
        else{
            float resultat = gestionDate.avancementProgressBar(formation.getSemestrePair().getDateDebut(), formation.getSemestrePair().getDateFin());
            pbSemestre2.setProgress(resultat); 
        }
        
        
        int column = 0;
        int row = 1;

        try {
            for(int i=0; i<this.listeUEIndex.size();i++){
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("UEProgress.fxml"));

                VBox ueProgress = fxmlLoader.load();
                UEProgressController ueProgressController = fxmlLoader.getController();
                ueProgressController.initialiserProgressBarUE(listeUEIndex.get(i), formation, this.numEtudiantCourant);

                //Style des Grid UEProgress
                listUEGridS1.setStyle("-fx-background-color:#FBE0C3;");
                listUEGridS2.setStyle("-fx-background-color:#FBE0C3;");
                
                if(column == 1){
                    column = 0;
                    row++;
                }
                
                int t = listeUEIndex.get(i);
                
                if (App.ListeUE.get(t).getSemestre().equals("1")) {
                    listUEGridS1.add(ueProgress, column++, row);
                }

                else {
                    listUEGridS2.add(ueProgress, column++, row);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    /**
     * Recuperation de la liste des UE pour un etudiant
     * @param numEtudiant
     * numero etudiant de l'étudiant dont on veut la liste des UE
     */
    public void recuperationListeUEEtudiant(String numEtudiant){
        this.numEtudiantCourant = numEtudiant;
        List<Integer> listeUEIndex = new ArrayList<>();
        
        for(int i=0; i<App.ListeUE.size(); i++){
            String listeNumEtudiant = App.ListeUE.get(i).getListeNumEtudiant();
            String[] words = listeNumEtudiant.split("/");
            for (int j=0; j<words.length; j++) {
                
                if (words[j].equals(numEtudiant)) {
                    listeUEIndex.add(i);
                }
            }
        }
        this.listeUEIndex = listeUEIndex;
  
    }
    
}
