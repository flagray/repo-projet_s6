/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import javafx.scene.layout.AnchorPane;

/**
 * Ecouteur pour savoir si un etudiant est cliqué par l'utilisateur
 */
public interface Listener {

    /**
     * permet de gérer le clic sur les card etudiant
     * @param etudiant
     * Etudiant sur lequel on clic
     * @param formation
     * formation ou est stocké l'etudiant
     * @param anchorPane
     * element FXML contenant l'element parent de la page avant le clic
     */
    
    public void onClickListener(Etudiant etudiant, Formation formation, AnchorPane anchorPane);
}
