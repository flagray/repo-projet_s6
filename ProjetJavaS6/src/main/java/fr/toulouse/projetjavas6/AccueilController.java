/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Formation;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * Controller du fichier FXML Accueil
 */
public class AccueilController implements Initializable {

    @FXML
    private VBox backVbox;

    /**
     * Initializes the controller class.
     * @param url
     * url
     * @param rb
     * rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.affichageContainerFormation();
    }  
    
    /**
     * Permet l'affichage du Fichier FXML ContainerFormationAccueil
     * 
     */
    public void affichageContainerFormation(){
        //Liste permettant l'affichage de licence, master,.. sur l'interface graphique
        List<String> listeNomDiplome = new ArrayList<>();
        
        //Ajout à une liste listeNomDiplome si le diplome n'est pas encore référencé
        for (Formation f : App.ListeFormation) {
            if (!listeNomDiplome.contains(f.getDiplome().getNomDiplome())){
                listeNomDiplome.add(f.getDiplome().getNomDiplome());
            }
        }
        
        
        //affichage des éléments dynamique
        try {
            
            ScrollPane backScrollPane = new ScrollPane();
            backScrollPane.setPrefSize(1290,750);
            
            //Style Vbox
            this.backVbox.setVgrow(backScrollPane,Priority.ALWAYS);
            
            backVbox.getChildren().add(backScrollPane);
            
            GridPane backGridPane = new GridPane();
            backScrollPane.setContent(backGridPane);
            
            //Style GridPane
            backGridPane.setStyle("-fx-background-color:white;");
            
            int column = 0;
            int row = 1;
            
            for(int i=0; i<listeNomDiplome.size();i++){
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("ContainerFormationAccueil.fxml"));
                AnchorPane anch1 = fxmlLoader.load();
                
                //Style de la AnchorPane
                anch1.setStyle("-fx-background-color:white;");
                
                //pour gerer l'interieur des liste diplome on appelle le controlleur de containerFormation
                ContainerFormationAccueilController containerFormationAccueil = fxmlLoader.getController();
                //et ses méthodes
                containerFormationAccueil.passerElem(this.backVbox);//permet de vider la fenetre quand on clique
                containerFormationAccueil.setNomFormation(listeNomDiplome.get(i));
                containerFormationAccueil.affichageCardSpecialite();
                
                
                
                if(column == 1){
                    column = 0;
                    row++;
                }
                
                backGridPane.add(anch1, column++, row);
                backGridPane.setMargin(anch1, new Insets(0, 0, 0, 50));
            }
            
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
