package fr.toulouse.projetjavas6;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */


import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.UniteEnseignement;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 * Controller du fichier FXML CardUE
 */
public class CardUEController{


    @FXML
    private Label lbNomUE;
    @FXML
    private Label lbCodeUE;
    
    private UniteEnseignement UE;
    
    private ListenerUE listenerUE;
    
    private Formation formation;

    
    
    @FXML
    private void click(MouseEvent event) {
        listenerUE.onClickListener(UE, this.formation);
    }

    /**
     * gère les données affichées sur les card UE
     * @param UE
     * UE courante
     * @param listenerUE
     * Mise a jour du ListenerUE
     */
    public void setDataCardUE(UniteEnseignement UE, ListenerUE listenerUE){
        this.UE = UE;
        this.listenerUE = listenerUE;
        
        lbNomUE.setText(UE.getNomUE());
        lbCodeUE.setText(UE.getCodeUE());
    }
    
    /**
     * Permet de faire passer la formation courante
     * @param formation
     * formation courante
     */
    public void sendData(Formation formation){
        this.formation = formation;
    }

}
