/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6;


import fr.toulouse.projetjavas6.Backend.Etudiant;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML AjoutEtudiant
 */
public class AjoutEtudiantController {

    @FXML
    private TextField tfNom;
    @FXML
    private TextField tfPrenom;
    
    @FXML
    private AnchorPane backAnchorPane;
    @FXML
    private Button boutonSuivant;
    
    /**
     *
     */
    public Etudiant etu = new Etudiant();
    
    
    
    /**
     * verifie si les données insérées pour le prenom et le nom de l'etudiant sont correctes
     * @return boolean true si les données saisies sont correctes ou si les champs ne sont pas vides sinon false.
     * 
     * 
     */
    private boolean isInputValid() {
        String erreur = "";

        if (this.tfNom.getText() == null || this.tfNom.getText().length() == 0) {
            erreur += "Nom invalide!\n";
        }
        if (this.tfPrenom.getText() == null || this.tfPrenom.getText().length() == 0) {
            erreur += "Prénom invalide!\n";
        }
        
        if (erreur.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(erreur);

            alert.showAndWait();

            return false;
        }
    }

    /**
     *
     * @throws IOException
     * Si le fichier n'est pas chargé correctement
     */
    @FXML 
    /**
     * ajoute l'etudiant dans la liste etudiant et change son nom et son prenom par les champs remplis
     * @throws exception pour le changement de page. 
     * 
     */
    public void handleSuivant() throws IOException {
        App.ListeEtudiant.add(etu);
        int longueur = App.ListeEtudiant.size();
        if (isInputValid()) {
            backAnchorPane.getChildren().add(boutonSuivant);
          
            try {
                App.ListeEtudiant.get(longueur-1).setNom(tfNom.getText().toUpperCase());
                App.ListeEtudiant.get(longueur-1).setPrenom(tfPrenom.getText().toUpperCase());
     
                this.tfNom.setText(null);
                this.tfPrenom.setText(null);
                
                FXMLLoader loader = new FXMLLoader(getClass().getResource("AjoutEtudiant_1.fxml"));
                Parent root = loader.load();
 
                Stage stage = (Stage) boutonSuivant.getScene().getWindow();
                stage.setScene(new Scene(root,1290, 750));

            } catch (IOException ex) {
                ex.printStackTrace();
            }

           
        }
    }
    
    
    

    
}
