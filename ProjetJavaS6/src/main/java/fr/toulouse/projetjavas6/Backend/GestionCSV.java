/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6.Backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe permettant la lecture et l'écriture dans les fichiers CSV
 */
public class GestionCSV {
    
    /**
     * Permet la lecture dans le fichier CSV de la liste d'etudiant
     * @param nomFichier
     * chemin du fichier de la liste d'etudiant
     * @return
     * retourne une List d'etudiant par rapport au fichier CSV
     */
    public List<Etudiant> lectureCSVListeEtudiant (String nomFichier){
        String line = "";
        List <Etudiant> listeEtudiant = new ArrayList <Etudiant> ();

        try (BufferedReader br = new BufferedReader(new FileReader(nomFichier))){

            if (br.readLine() == null) {
                System.out.println("FICHIER VIDE");
                //TODO : ajouter une exception personnalisée
            }

            else{
                int i=0;
                while((line = br.readLine()) != null) {

                    //pour chaque ligne du fichier on ajoute une liste
                    //pour avoir les info d'un étudiant
                    // représentation : listeEtudiant[[numEtu, prenom, nom], [numEtu, prenom, nom], [numEtu, prenom, nom]]
                    listeEtudiant.add(new Etudiant());

                    String[] words = line.split(",");

                    for (int j=0; j<words.length; j++) {

                        switch (j) {
                            case 0:
                                int numEtu = Integer.parseInt(words[j]);
                                listeEtudiant.get(i).setNumeroEtudiant(numEtu);
                                break;
                            case 1:
                                listeEtudiant.get(i).setNom(words[j]);
                            case 2:
                                listeEtudiant.get(i).setPrenom(words[j]);
                                break;
                            case 3:
                                listeEtudiant.get(i).setImg(words[j]);
                                break;
                            case 4:
                                listeEtudiant.get(i).setMail(words[j]);
                                break;
                            case 5:
                                int nbECTS = Integer.parseInt(words[j]);
                                listeEtudiant.get(i).setNbECTS(nbECTS);
                                break;
                            case 6:
                                int niveau = Integer.parseInt(words[j]);
                                listeEtudiant.get(i).setNiveau(niveau);
                                break;
                        }
                    }

                    i++;
                }

                return listeEtudiant;
            }

        } catch (IOException ex) {
            System.out.println("FICHIER INEXISTANT");return null;
        }

        return null;
    }
    
    /**
     * Permet l'ecriture dans le fichier CSV de la liste d'etudiant
     * @param nomFichier
     * chemin du fichier de la liste d'etudiant
     * @param listeEtudiant
     * liste d'etudiant extraite à la lecture et modifié lors des traitements de l'application
     */
    public void ecritureCSVetudiant(String nomFichier, List <Etudiant> listeEtudiant){
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(new File(nomFichier)))) {
            StringBuilder sb = new StringBuilder();
            sb.append("numEtudiant,nom,prenom,img,mail,ECTS,\n");
            
            for(int i=0; i<listeEtudiant.size(); i++){
                sb.append(listeEtudiant.get(i).getNumeroEtudiant()+",");
                sb.append(listeEtudiant.get(i).getNom()+",");
                sb.append(listeEtudiant.get(i).getPrenom()+",");
                sb.append(null+",");
                sb.append(listeEtudiant.get(i).getMail()+",");
                sb.append(listeEtudiant.get(i).getNbECTS()+",");
                sb.append(listeEtudiant.get(i).getNiveau()+",");
                
                
                if(i<listeEtudiant.size()-1){
                    sb.append("\n");
                } 
            }
            
            writer.write(sb.toString());

            writer.flush();
            writer.close();

        } catch (FileNotFoundException e) {
            System.out.println("FICHIER INEXISTANT");
        }
        
    }
    
    /**
     * Permet la lecture dans le fichier CSV de la liste de de formation
     * @param nomFichier
     * chemin du fichier de la liste de formation
     * @return
     * retourne une liste de formation d'après le fichier CSV
     */
    public List<Formation> lectureCSVListeFormation (String nomFichier){
        String line = "";
        List <Formation> listeFormation = new ArrayList <Formation> ();

        try (BufferedReader br = new BufferedReader(new FileReader(nomFichier))){

            if (br.readLine() == null) {
                System.out.println("FICHIER VIDE");
                //TODO : ajouter une exception personnalisée
            }

            else{
                int i=0;
                while((line = br.readLine()) != null) {

                    //pour chaque ligne du fichier on ajoute une liste
                    //pour avoir les info d'une formation             ex:MIAGE   ex:Lience       ex:L1    ex:secience numerique
                    // représentation : listeFormation[[nomMention, nomParcours, nomDiplome, anneeDiplome, nomSpecialite        , dateDebutAnnee, dateFinAnnee, dateDebutSemestreImpair, dateFinSemestreImpair, dateDebutSemestrePair, dateFinSemestrePair]]
                    listeFormation.add(new Formation());

                    String[] words = line.split(",");
                    
                    Mention mention = new Mention();
                    Diplome diplome = new Diplome();
                    AnneeUniversitaire annee = new AnneeUniversitaire();
                    Semestre semestreImpair = new Semestre();
                    Semestre semestrePair = new Semestre();

                    for (int j=0; j<words.length; j++) {
                        
                         
                        switch (j) {
                            case 0:
                                mention.setNomMention(words[j]);
                                listeFormation.get(i).setMention(mention);
                                break;
                            case 1:
                                mention.setParcours(words[j]);
                                listeFormation.get(i).setMention(mention);
                                break;
                            case 2:
                                diplome.setNomDiplome(words[j]);
                                listeFormation.get(i).setDiplome(diplome);
                                break;
                            case 3:
                                listeFormation.get(i).setAnneeDiplome(words[j]);
                                break;
                            case 4:
                                mention.setSpecialite(words[j]);
                                listeFormation.get(i).setMention(mention);
                                break;
                            case 5:
                                annee.setDateDebut(words[j]);
                                listeFormation.get(i).setAnnee(annee);                             
                                break;
                            case 6:
                                annee.setDateFin(words[j]);
                                listeFormation.get(i).setAnnee(annee);
                                break;
                            case 7:
                                semestreImpair.setDateDebut(words[j]);
                                listeFormation.get(i).setSemestreImpair(semestreImpair);
                                break;
                            case 8:
                                semestreImpair.setDateFin(words[j]);
                                listeFormation.get(i).setSemestreImpair(semestreImpair);
                                break;
                            case 9:
                                semestrePair.setDateDebut(words[j]);
                                listeFormation.get(i).setSemestrePair(semestrePair);
                                break;
                            case 10:
                                semestrePair.setDateFin(words[j]);
                                listeFormation.get(i).setSemestrePair(semestrePair);
                                break;
                            case 11:
                                listeFormation.get(i).setListeNumEtudiant(words[j]);
                                break;
                            case 12:
                                listeFormation.get(i).setListeUE(words[j]);
                                break;
                        }
                        
                    }

                    i++;
                }

                return listeFormation;
            }

        } catch (IOException ex) {
            System.out.println("FICHIER INEXISTANT");return null;
        }

        return null;
    }

    /**
     * Permet l'ecriture dans le fichier CSV de la liste de formation
     * @param nomFichier
     * chemin du fichier de la liste de formation
     * @param listeFormation
     * liste de formation extraite à la lecture et modifié lors des traitements de l'application
     */
    public void ecritureCSVformation(String nomFichier, List <Formation> listeFormation){
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(new File(nomFichier)))) {
            StringBuilder sb = new StringBuilder();
            sb.append("Mention,Parcours,Diplome,AnneeDiplome,Specialite,"
                    + "DateDebutAnnee,DateFinAnnee,DebutSemestreImpair,FinSemestreImpair,"
                    + "DebutSemestrePair,FinSemestrePair,EtudiantInscrit,UE\n");
            
            for(int i=0; i<listeFormation.size(); i++){
                sb.append(listeFormation.get(i).getMention().getNomMention()+",");
                sb.append(listeFormation.get(i).getMention().getParcours()+",");
                sb.append(listeFormation.get(i).getDiplome().getNomDiplome()+",");
                sb.append(listeFormation.get(i).getAnneeDiplome()+",");
                sb.append(listeFormation.get(i).getMention().getSpecialite()+",");
                sb.append(listeFormation.get(i).getAnnee().getDateDebut()+",");
                sb.append(listeFormation.get(i).getAnnee().getDateFin()+",");
                sb.append(listeFormation.get(i).getSemestreImpair().getDateDebut()+",");
                sb.append(listeFormation.get(i).getSemestreImpair().getDateFin()+",");
                sb.append(listeFormation.get(i).getSemestrePair().getDateDebut()+",");
                sb.append(listeFormation.get(i).getSemestrePair().getDateFin()+",");
                sb.append(listeFormation.get(i).getListeNumEtudiant()+",");
                sb.append(listeFormation.get(i).getListeUE()+",");

                
                if(i<listeFormation.size()-1){
                    sb.append("\n");
                } 
            }
            
            writer.write(sb.toString());

            writer.flush();
            writer.close();

        } catch (FileNotFoundException e) {
            System.out.println("FICHIER INEXISTANT");
        }
        
    }
    
    /**
     * Permet la lecture dans le fichier CSV de la liste des UE
     * @param nomFichier
     * chemin du fichier de la liste des UE
     * @return
     * retourne une liste d'UE d'après le fichier CSV
     */
    public List<UniteEnseignement> lectureCSVListeUE (String nomFichier){
        String line = "";
        List <UniteEnseignement> listeUE = new ArrayList <UniteEnseignement> ();

        try (BufferedReader br = new BufferedReader(new FileReader(nomFichier))){

            if (br.readLine() == null) {
                System.out.println("FICHIER VIDE");
                //TODO : ajouter une exception personnalisée
            }

            else{
                int i=0;
                while((line = br.readLine()) != null) {

                    //pour chaque ligne du fichier on ajoute une liste
                    
                    listeUE.add(new UniteEnseignement());

                    String[] words = line.split(",");

                    for (int j=0; j<words.length; j++) {

                        switch (j) {
                            case 0:
                                listeUE.get(i).setCodeUE(words[j]);
                                break;
                            case 1:
                                listeUE.get(i).setNomUE(words[j]);
                                break;
                            case 2:
                                int nbECTS = Integer.parseInt(words[j]);
                                listeUE.get(i).setNbECTS(nbECTS);
                                break;
                            case 3:
                                boolean ouverture = Boolean.parseBoolean(words[j]);
                                listeUE.get(i).setOuverture(ouverture);
                                break;
                                
                            case 4:
                                listeUE.get(i).setPrerequis(words[j]);
                                break;
                            case 5:
                                listeUE.get(i).setListeNumEtudiant(words[j]);
                                break;
                            case 6:
                                listeUE.get(i).setListeNumEtudiantValide(words[j]);
                                break;
                            case 7:
                                listeUE.get(i).setSemestre(words[j]);
                                break;
                                
                            case 8:
                                int creneau = Integer.parseInt(words[j]);
                                listeUE.get(i).setCreneau(creneau);
                                break;
                        }
                    }

                    i++;
                }

                return listeUE;
            }

        } catch (IOException ex) {
            System.out.println("FICHIER INEXISTANT");return null;
        }

        return null;
    }
    
    /**
     * Permet l'ecriture dans le fichier CSV de la liste d'UE
     * @param nomFichier
     * chemin du fichier de la liste d'UE
     * @param listeUE
     * liste d'UE extraite à la lecture et modifié lors des traitements de l'application
     */
    public void ecritureCSVUE(String nomFichier, List <UniteEnseignement> listeUE){
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(new File(nomFichier)))) {
            StringBuilder sb = new StringBuilder();
            sb.append("codeUE,nomUE,nbECTS,ouverture,prerequis,listeNumEtudiantInscrit,listeNumEtudiantValide,Semestre,creneau\n");
            
            for(int i=0; i<listeUE.size(); i++){
                sb.append(listeUE.get(i).getCodeUE()+",");
                sb.append(listeUE.get(i).getNomUE()+",");
                sb.append(listeUE.get(i).getNbECTS()+",");
                sb.append(listeUE.get(i).isOuverture()+",");
                sb.append(listeUE.get(i).getPrerequis()+",");
                sb.append(listeUE.get(i).getListeNumEtudiant()+",");
                sb.append(listeUE.get(i).getListeNumEtudiantValide()+",");
                sb.append(listeUE.get(i).getSemestre()+",");
                sb.append(listeUE.get(i).getCreneau()+",");
                
                if(i<listeUE.size()-1){
                    sb.append("\n");
                } 
            }
            
            writer.write(sb.toString());

            writer.flush();
            writer.close();

        } catch (FileNotFoundException e) {
            System.out.println("FICHIER INEXISTANT");
        }
        
    }
    
    
}    

