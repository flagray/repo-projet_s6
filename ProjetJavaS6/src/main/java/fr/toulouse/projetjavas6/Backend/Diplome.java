/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6.Backend;

/**
 * Classe métier d'un diplome
 */
public class Diplome {
    private String nomDiplome;

    /**
     * Getter nom du diplome
     * @return le nom du diplome
     */
    public String getNomDiplome() {
        return nomDiplome;
    }

    /**
     * Setter du nom du diplome
     * @param nomDiplome
     * nom du diplome à initialiser
     */
    public void setNomDiplome(String nomDiplome) {
        this.nomDiplome = nomDiplome;
    }

    
}
