/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6.Backend;

/**
 * Classe métier d'une formation
 */
public class Formation{
    private String anneeDiplome;
    private AnneeUniversitaire annee;
    private Semestre semestreImpair;
    private Semestre semestrePair;
    private Diplome diplome;
    private Mention mention;
    private String listeNumEtudiant;
    private String listeUE;
    
    /**
     * Getter année du diplome de la formation 
     * @return
     */
    public String getAnneeDiplome() {
        return anneeDiplome;
    }

    /**
     * Getter de l'année universitaire de la formation
     * @return
     */
    public AnneeUniversitaire getAnnee() {
        return annee;
    }

    /**
     * Getter du semestre Impair
     * @return
     */
    public Semestre getSemestreImpair() {
        return semestreImpair;
    }

    /**
     * Getter du semestre Pair
     * @return
     */
    public Semestre getSemestrePair() {
        return semestrePair;
    }

    /**
     * Getter du diplome de la formation
     * @return
     */
    public Diplome getDiplome() {
        return diplome;
    }

    /**
     * Getter de la mention de la formation
     * @return
     */
    public Mention getMention() {
        return mention;
    }

    /**
     * Getter de la liste de numero Etudiant
     * @return
     */
    public String getListeNumEtudiant() {
        return listeNumEtudiant;
    }

    /**
     * Getter de la liste d'UE
     * @return
     */
    public String getListeUE() {
        return listeUE;
    }
    
    /**
     * Setter année du diplome de la formation  
     * @param anneeDiplome
     */
    public void setAnneeDiplome(String anneeDiplome) {
        this.anneeDiplome = anneeDiplome;
    }

    /**
     * Setter de l'année universitaire de la formation
     * @param annee
     */
    public void setAnnee(AnneeUniversitaire annee) {
        this.annee = annee;
    }

    /**
     * Setter du semestre Impair
     * @param semestreImpair
     */
    public void setSemestreImpair(Semestre semestreImpair) {
        this.semestreImpair = semestreImpair;
    }

    /**
     * Setter du semestre Pair
     * @param semestrePair
     */
    public void setSemestrePair(Semestre semestrePair) {
        this.semestrePair = semestrePair;
    }

    /**
     * Setter du diplome de la formation
     * @param diplome
     */
    public void setDiplome(Diplome diplome) {
        this.diplome = diplome;
    }

    /**
     * Setter de la mention de la formation
     * @param mention
     */
    public void setMention(Mention mention) {
        this.mention = mention;
    }

    /**
     * Setter de la liste de numero etudiant de la formation
     * @param listeNumEtudiant
     */
    public void setListeNumEtudiant(String listeNumEtudiant) {
        this.listeNumEtudiant = listeNumEtudiant;
    }

    /**
     * Setter d ela liste des UE de la formation
     * @param listeUE
     */
    public void setListeUE(String listeUE) {
        this.listeUE = listeUE;
    }
    
    /**
     * Redefinition de la méthode toString
     * @return 
     */
    public String toString(){
        String s = "Mention : " +getMention().getNomMention();
                s+= "\n Parcours : " + getMention().getParcours();
                s+= "\n Specialite : " + getMention().getSpecialite();
                s+= "\n Annee : " + getAnneeDiplome();
        
        return s;
    }

}
