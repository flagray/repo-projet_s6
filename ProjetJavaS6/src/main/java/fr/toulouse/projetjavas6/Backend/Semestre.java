/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6.Backend;

/**
 * Classe métier d'un semestre
 */
public class Semestre {
    private int numeroSemestre;
    private String dateDebut;
    private String dateFin;

    /**
     * Getter numero du semestre
     * @return
     */
    public int getNumeroSemestre() {
        return numeroSemestre;
    }

    /**
     * Getter date du début du semstre
     * @return
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     * Getter date de la fin du semestre
     * @return
     */
    public String getDateFin() {
        return dateFin;
    }

    /**
     * Setter numero du semestre
     * @param numeroSemestre
     */
    public void setNumeroSemestre(int numeroSemestre) {
        this.numeroSemestre = numeroSemestre;
    }

    /**
     * Setter date du début du semestre
     * @param dateDebut
     */
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Setter date de fin du semstre
     * @param dateFin
     */
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }
    
    
    
}
