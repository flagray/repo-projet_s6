/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6.Backend;

import java.util.List;

/**
 * Classe métier d'une mention
 */
public class Mention {
    private String nomMention;
    private String specialite;
    private String Parcours;

    /**
     * Getter du nom de la mention
     * @return
     */
    public String getNomMention() {
        return nomMention;
    }

    /**
     * Getter du nom de la specialite
     * @return
     */
    public String getSpecialite() {
        return specialite;
    }

    /**
     * Getter du nom du parcours
     * @return
     */
    public String getParcours() {
        return Parcours;
    }

    /**
     * Setter du nom de la mention
     * @param nomMention
     */
    public void setNomMention(String nomMention) {
        this.nomMention = nomMention;
    }

    /**
     * Setter du nom de la specialite
     * @param specialite
     */
    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    /**
     * Setter du parcours de la mention
     * @param parcours
     */
    public void setParcours(String parcours) {
        this.Parcours = parcours;
    }

    
    
    
    
}
