/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6.Backend;

/**
 * Classe métier d'un etudiant
 */
public class Etudiant {
    private int numeroEtudiant;
    private String nom;
    private String prenom;
    private String img;
    private String mail;
    private int nbECTS;
    private int niveau;

    /**
     * Getter numero etudiant
     * @return numero etudiant
     */
    public int getNumeroEtudiant() {
        return numeroEtudiant;
    }

    /**
     * Getter nom etudiant
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     * Getter prenom etudiant
     * @return
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Getter mail etudiant
     * @return
     */
    public String getMail() {
        return mail;
    }

    /**
     * Getter nombre ECTS etudiant
     * @return
     */
    public int getNbECTS() {
        return nbECTS;
    }
    
    /**
     * Getter nombre d'année d'etude d'un etudiant
     * @return
     */
    public int getNiveau() {
        return niveau;
    }
    
    /**
     * Setter numero etudiant 
     * @param numeroEtudiant
     */
    public void setNumeroEtudiant(int numeroEtudiant) {
        this.numeroEtudiant = numeroEtudiant;
    }

    /**
     * Setter nom etudiant
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Setter prenom etudiant
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    /**
     * Setter image etudiant
     * @param img
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * Setter mail etudiant
     * @param mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * Setter nombre ECTS etudiant
     * @param nbECTS
     */
    public void setNbECTS(int nbECTS) {
        this.nbECTS = nbECTS;
    }
   
    /**
     * Setter nombre d'année d'etude etudiant
     * @param niveau
     */
    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }
    
}
