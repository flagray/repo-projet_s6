/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6.Backend;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * Classe permettant de gérer les dates et leurs actualisations à chaque lancement de l'application
 */
public class GestionDate {
    
    
    public LocalDate todayDate = LocalDate.now();
    
    /**
     * calcul du nombre de jour entre 2 date
     * @param dateDebut
     * @param dateFin
     * @return
     * retourne l'écart entre 2 dates
     */
    public float ecartDate(String dateDebut, String dateFin) {
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         //"MM/dd/yyyy"
        try {
            Date dateAvant = sdf.parse(dateDebut);
            Date dateApres = sdf.parse(dateFin);
            long diff = dateApres.getTime() - dateAvant.getTime();
            float res = (diff / (1000*60*60*24));
            //System.out.println("Nombre de jours entre les deux dates est: "+res);
            return res;
        } catch (Exception e) {
         e.printStackTrace();
        }
        
        return 0;
    }
    
    /**
     * Permet de convertir l'écart entre 2 dates en une valeur entre 0 et 1 pour l'affichage des barres de progressions
     * @param dateDebut
     * @param dateFin
     * @return
     * retourne une valeur entre 0 et un pour l'affichage des barres de progressions
     */
    public float avancementProgressBar(String dateDebut, String dateFin){
        
        float resultat = (100*
                          this.ecartDate(dateDebut, this.todayDate.toString())/
                          this.ecartDate(dateDebut, dateFin)
                          )/100;
        
        return resultat;
        
        
    }
    
    
}
