/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6.Backend;

/**
 * Classe métier de l'année universitaire
 */
public class AnneeUniversitaire {
    private String dateDebut;
    private String dateFin;

    /**
     * Setter dateDebut
     * @param dateDebut
     */
    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    /**
     * Setter dateFin
     * @param dateFin
     */
    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Getter dateDebut
     * @return dateDebut
     */
    public String getDateDebut() {
        return dateDebut;
    }

    /**
     * Getter dateFin
     * @return dateFIn
     */
    public String getDateFin() {
        return dateFin;
    }
}
