/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6.Backend;

/**
 * Classe métier d'un semestre
 */
public class UniteEnseignement {
    private String codeUE;
    private String nomUE;
    private int nbECTS;
    private boolean ouverture;
    private String prerequis;
    private String listeNumEtudiant;
    private String listeNumEtudiantValide;
    private String semestre;
    private int creneau;

    /**
     * Getter du code de l'UE
     * @return
     */
    public String getCodeUE() {
        return codeUE;
    }

    /**
     * Getter du nnom de l'UE
     * @return
     */
    public String getNomUE() {
        return nomUE;
    }

    /**
     * Getter du nombre d'ECTS de l'UE
     * @return
     */
    public int getNbECTS() {
        return nbECTS;
    }

    /**
     * Getter pour savoir si une UE est une UE d'ouverture ou non
     * @return
     */
    public boolean isOuverture() {
        return ouverture;
    }

    /**
     * Getter pour connaitre la liste des prerequis dans le cas ou l'UE est une UE d'ouverture
     * @return
     */
    public String getPrerequis() {
        return prerequis;
    }

    /**
     * Getter pour connaitre la liste des numéro etudiant de l'UE
     * @return
     */
    public String getListeNumEtudiant() {
        return listeNumEtudiant;
    }

    /**
     * Getter pour connaitre la liste des numero etudiant qui on validé l'UE
     * @return
     */
    public String getListeNumEtudiantValide() {
        return listeNumEtudiantValide;
    }

    /**
     * Getter du semestre de l'UE
     * @return
     */
    public String getSemestre() {
        return semestre;
    }
    
    /**
     * Getter du créneau de l'UE
     * @return
     */
    public int getCreneau(){
        return this.creneau;
    }

    /**
     * Setter du code de l'UE
     * @param codeUE
     */
    public void setCodeUE(String codeUE) {
        this.codeUE = codeUE;
    }

    /**
     * Setter du nom de l'UE
     * @param nomUE
     */
    public void setNomUE(String nomUE) {
        this.nomUE = nomUE;
    }

    /**
     * Setter du nombre d'ECTS de l'UE
     * @param nbECTS
     */
    public void setNbECTS(int nbECTS) {
        this.nbECTS = nbECTS;
    }

    /**
     * Setter pour qu'une UE soit d'ouverture ou pas
     * @param ouverture
     */
    public void setOuverture(boolean ouverture) {
        this.ouverture = ouverture;
    }

    /**
     * Setter pour modifier les prerequis d'une UE
     * @param prerequis
     */
    public void setPrerequis(String prerequis) {
        this.prerequis = prerequis;
    }

    /**
     * Setter pour modifier la liste de numéro etudiant d'une UE
     * @param listeNumEtudiant
     */
    public void setListeNumEtudiant(String listeNumEtudiant) {
        this.listeNumEtudiant = listeNumEtudiant;
    }

    /**
     * Setter pour modifier la liste d'etudiant qui ont validé
     * @param listeNumEtudiantValide
     */
    public void setListeNumEtudiantValide(String listeNumEtudiantValide) {
        this.listeNumEtudiantValide = listeNumEtudiantValide;
    }

    /**
     * Setter pour modifier le semestre d'une UE
     * @param semestre
     */
    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }
    
    /**
     * Setter pour modifier les créneaux d'une UE
     * @param creneau
     */
    public void setCreneau (int creneau){
        this.creneau = creneau;
    }

    
    
}
