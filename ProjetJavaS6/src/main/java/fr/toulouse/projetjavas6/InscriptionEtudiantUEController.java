/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.GestionCSV;
import fr.toulouse.projetjavas6.Backend.UniteEnseignement;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;

import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML InscriptionEtudiantUE
 */
public class InscriptionEtudiantUEController{


    @FXML
    private VBox backVbox;
    @FXML
    private ListView<CheckBox> ListViewEtudiant;
    @FXML
    private Button button;
        
    
    /**
     * Permet de gérer l'inscription d'un etudiant dans une UE
     * @param UE
     * UE courante
     * @param formation 
     * formation courante
     */
    public void inscriptionEtudiant(UniteEnseignement UE, Formation formation){
        
        List<Etudiant> listeEtudiantInscrit = new ArrayList<>();
        List<Etudiant> listeEtudiantNonInscrit = new ArrayList<>();
        
        
        //recuperation de la liste d'étudiant inscrit à l'ue
        String[] numeroEtudiantUE = UE.getListeNumEtudiant().split("/");
       
        for(int i=0; i<App.ListeEtudiant.size();i++){
            String numeroEtudiant = String.valueOf(App.ListeEtudiant.get(i).getNumeroEtudiant());
            for (int j=0; j<numeroEtudiantUE.length; j++) {
                if (numeroEtudiantUE[j].equals(numeroEtudiant)) {
                    listeEtudiantInscrit.add(App.ListeEtudiant.get(i));
                }
            }
        }
        
        //recuperation de la liste d'étudiant inscrit à la formation
        String[] numeroEtudiantFormation = formation.getListeNumEtudiant().split("/");
       
        for(int i=0; i<App.ListeEtudiant.size();i++){
            String numeroEtudiant = String.valueOf(App.ListeEtudiant.get(i).getNumeroEtudiant());
            for (int j=0; j<numeroEtudiantFormation.length; j++) {
                if (numeroEtudiantFormation[j].equals(numeroEtudiant)) {
                    listeEtudiantNonInscrit.add(App.ListeEtudiant.get(i));
                }
            }
        }
        
        //on enleve à la liste d'étudiant de la formation les étudiant déjà inscrit
        for (int i = 0; i < listeEtudiantNonInscrit.size(); i++) { 
            for (int j = 0; j < listeEtudiantInscrit.size(); j++) {
                String etudiantNonInscrit = String.valueOf(listeEtudiantNonInscrit.get(i).getNumeroEtudiant());
                String etudiantInscrit = String.valueOf(listeEtudiantInscrit.get(j).getNumeroEtudiant());
                
                if (etudiantInscrit.equals(etudiantNonInscrit)) {
                    listeEtudiantNonInscrit.remove(i);
                }
            }
        }
        
        List<Etudiant> listeEtudiantPrerequis = new ArrayList<>();
        List<Etudiant> listeEtudiantAafficher = new ArrayList<>();
        
        if (UE.isOuverture()) {
            
            listeEtudiantPrerequis.addAll(this.gestionPrerequis(UE));
            
            for(int i=0; i<listeEtudiantNonInscrit.size(); i++){
                if(listeEtudiantPrerequis.contains(listeEtudiantNonInscrit.get(i))){
                    listeEtudiantAafficher.add(listeEtudiantNonInscrit.get(i));
                }
            }
            
        }
        
        else{
            listeEtudiantAafficher.addAll(listeEtudiantNonInscrit);
        }
        

        //Style ListView
        ListViewEtudiant.setStyle("-fx-font-family:Calibri Light; -fx-control-inner-background: #F5F5F5; -fx-control-inner-background-alt: derive(-fx-control-inner-background, 50%);");
        
        for (int i = 0; i < listeEtudiantAafficher.size(); i++) {
            String numEtudiant = String.valueOf(listeEtudiantAafficher.get(i).getNumeroEtudiant());
            CheckBox check = new CheckBox(numEtudiant);
            ListViewEtudiant.getItems().add(check);
        }
        
    
        this.validerInscription(UE);

        
    }
    
    /**
     * Inscription de l'etudiant dans l'UE courante avec ecriture dans le fichier CSV
     * @param UE 
     * UE courante
     */
    public void validerInscription(UniteEnseignement UE){
        button.setOnAction(event -> {
                                    String nouvelleListe ="";
                                    for (CheckBox box : ListViewEtudiant.getItems()) {
                                        if (box.isSelected()) {
                                            nouvelleListe+= box.getText()+"/";
                                        }
                                    }
            
                                    for(int i=0; i<App.ListeUE.size();i++){
                                        if (App.ListeUE.get(i).getCodeUE().equals(UE.getCodeUE())) {
                                            String listeEtudiantExistante = App.ListeUE.get(i).getListeNumEtudiant();
                                            if(listeEtudiantExistante.equals("null")){
                                               App.ListeUE.get(i).setListeNumEtudiant(nouvelleListe); 
                                            }
                                            else{
                                                App.ListeUE.get(i).setListeNumEtudiant(listeEtudiantExistante+nouvelleListe);
                                            }
                                            
                                        }
                                    }
                                    
                                    GestionCSV g1 = new GestionCSV();
                                    
                                    
                                    g1.ecritureCSVUE("donnee_application/UE.csv", App.ListeUE);
                                    
                                    
                                    Stage stage = (Stage) button.getScene().getWindow();
                                    stage.close();
                                    });
    }
    
        /**
     * Permet de gérer la notion de prérequis
     * @param UE
     * indique l'UE courante
     * @return
     * retourne la liste d'étudiant avec des prérequis
     */
    public List<Etudiant> gestionPrerequis(UniteEnseignement UE){

        List<UniteEnseignement> listeUEPrerequis = new ArrayList<>();

        List<Etudiant> listeEtudiantAvecPrerequis = new ArrayList<>();

        List<Etudiant> listeEtudiantAvecPrerequisPossible = new ArrayList<>();

        String[] listePrerequis = UE.getPrerequis().split("/");


        for(int i=0; i<App.ListeUE.size();i++){
            String codeUE = String.valueOf(App.ListeUE.get(i).getCodeUE());
            for (int j=0; j<listePrerequis.length; j++) {
                if (listePrerequis[j].equals(codeUE)) {
                    listeUEPrerequis.add(App.ListeUE.get(i));
                }
            }
        }

        for(int b=0; b<listeUEPrerequis.size();b++){
            //recuperation de la liste d'étudiant qui ont validé l'UE
            String[] numeroEtudiantValide = listeUEPrerequis.get(b).getListeNumEtudiantValide().split("/");

            for(int i=0; i<App.ListeEtudiant.size();i++){
                String numeroEtudiant = String.valueOf(App.ListeEtudiant.get(i).getNumeroEtudiant());
                for (int j=0; j<numeroEtudiantValide.length; j++) {
                    if (numeroEtudiantValide[j].equals(numeroEtudiant) && !listeEtudiantAvecPrerequis.contains(App.ListeEtudiant.get(i))) {
                        //listeEtudiantAvecPrerequis.add(App.ListeEtudiant.get(i));
                    }

                    if(numeroEtudiantValide[j].equals(numeroEtudiant) && b==0){
                        listeEtudiantAvecPrerequisPossible.add(App.ListeEtudiant.get(i));
                    }

                    else if (numeroEtudiantValide[j].equals(numeroEtudiant)){
                        if(listeEtudiantAvecPrerequisPossible.contains(App.ListeEtudiant.get(i)) && !listeEtudiantAvecPrerequis.contains(App.ListeEtudiant.get(i))){
                            listeEtudiantAvecPrerequis.add(App.ListeEtudiant.get(i));
                        }
                    }


                }
            }
        }

        if(listeUEPrerequis.size()==0){
            listeEtudiantAvecPrerequis = listeEtudiantAvecPrerequisPossible;
        }
        
        return listeEtudiantAvecPrerequis;
    }
    
    /**
     *
     * Permet de retourner à la page précédente
     * @param stage
     * Stage de la sccene précédente
     * @param scene
     * Scene de la scene précédente
     */
    public void affichageButtonRetour(Stage stage, Scene scene){
        //Gestion du bouton retour
        Button backButton = new Button("Retour");
        backVbox.getChildren().add(backButton);

        //Style du bouton de retour
        backButton.setStyle("-fx-background-color:none; -fx-font-size:28.0");
        backButton.setText("↩");
        backButton.setLayoutX(12.0);
        backButton.setLayoutY(60.0);
        
        backButton.setOnAction(event -> {
                            stage.setScene(scene);
                            });
    }

}
