package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.GestionCSV;
import fr.toulouse.projetjavas6.Backend.UniteEnseignement;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

/**
 * JavaFX App
 */
public class App extends Application {
    
    /**
     *
     */
    public static Scene scene;
    
    /**
     * Liste d'etudiant static
     */
    public static List<Etudiant> ListeEtudiant;
    /**
     * Liste dde formation static
     */
    public static List<Formation> ListeFormation;
    /**
     * Liste d'unite d'enseignement static
     */
    public static List<UniteEnseignement> ListeUE;

    /**
     * Permet de lancer l'application, point d'entrée de l'application
     * @param stage
     * @throws IOException 
     */
    @Override
    public void start(Stage stage) throws IOException {
        this.gestionMemoireEtudiant();
        this.gestionMemoireFormation();
        this.gestionMemoireUE(); 
        scene = new Scene(loadFXML("Accueil"), 1290, 750);
        stage.setScene(scene);
        stage.setTitle("Formation 2.0");
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }
    
    /**
     * initialise la liste d'etudiant static
     */
    public void gestionMemoireEtudiant(){
        GestionCSV g1 = new GestionCSV();
        this.ListeEtudiant = g1.lectureCSVListeEtudiant("donnee_application/ListeEtudiant.csv");
    }
     /**
      * initialise la liste de formation static
      */
    public void gestionMemoireFormation(){
        GestionCSV g1 = new GestionCSV();
        this.ListeFormation = g1.lectureCSVListeFormation("donnee_application/Formation.csv");
    }
    /**
     * initialise la liste des UE static
     */
    public void gestionMemoireUE(){
        GestionCSV g1 = new GestionCSV();
        this.ListeUE = g1.lectureCSVListeUE("donnee_application/UE.csv");
    }

    /**
     *
     * @param args
     * Methode Main
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}