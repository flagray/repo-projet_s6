/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.GestionCSV;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML AjoutEtudiant_1
 */
public class AjoutEtudiant_1Controller {

    @FXML
    private TextField tfNumEtudiant;
    @FXML
    private TextField tfMail;
    @FXML
    private AnchorPane ajoutEtudiantAP1;
    @FXML
    private Button boutonSuivant1;
    
    private int numEtu;
 
    GestionCSV csv = new GestionCSV();
    
    /** verifie si le numero etudiant et le mail sont bien insérés
     * @return boolean : true si données correctes sinon false
     */
    private boolean isInputValid() {
        String erreur = "";

        if (this.tfNumEtudiant.getText() == null || this.tfNumEtudiant.getText().length() == 0  ) {
            erreur += "Numéro Etudiant invalide!\n";
        }
        if (this.tfMail.getText() == null || this.tfMail.getText().length() == 0) {
            erreur += "Mail invalide!\n";
        }
        
        if (erreur.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de saisie");
            alert.setHeaderText("");
            alert.setContentText(erreur);

            alert.showAndWait();

            return false;
        }
    }

    @FXML
    /**
     * modifie le numero etudiant et le mail du dernier etudiant inscrit lorsqu'on clique sur le bouton suivant
     * @trows exception pour changer de page
     */
    private void handleSuivant() throws IOException {
        
        int longueur = App.ListeEtudiant.size();
        if (isInputValid()) {
            Etudiant etu = App.ListeEtudiant.get(longueur-1);
         
     
                
            numEtu = Integer.parseInt(tfNumEtudiant.getText());

            etu.setNumeroEtudiant(numEtu);
            etu.setMail(tfMail.getText());
            this.tfNumEtudiant.setText(null);
            this.tfMail.setText(null);
            
           
            
            
                            
           
            FXMLLoader loader = new FXMLLoader(getClass().getResource("AjoutEtudiant_2.fxml"));
            Parent root = loader.load();
         

            
            Stage stage = (Stage) boutonSuivant1.getScene().getWindow();
            stage.setScene(new Scene(root,1290, 750));
           
        }
    }
    
    
}
