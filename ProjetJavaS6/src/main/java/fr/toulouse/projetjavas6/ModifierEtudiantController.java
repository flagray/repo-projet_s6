/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.GestionCSV;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML ModifierEtudiant
 */
public class ModifierEtudiantController {

    @FXML
    private TextField modifPrenom;
    @FXML
    private TextField modifNom;
    @FXML
    private TextField modifMail;
    
    private Etudiant etudiant;
    private Formation formation;
    
    GestionCSV g1 = new GestionCSV();
    ProfilEtudiantController pEc = new ProfilEtudiantController();
    @FXML
    private Button btInscriptionS1;
    @FXML
    private Button btInscriptionS2;
    @FXML
    private Button btValider;
    
 
    /**
     * recupere l'etudiant à modifier
     * @param etu 
     * 
     */
    public void recupEtudiant(Etudiant etu){
        this.etudiant = etu;
    }

    @FXML
    public void handleValider(){
        int i=0;
        //recupererDonnees(pEc.getPEtudiantController());
        while (etudiant.getNumeroEtudiant()!=App.ListeEtudiant.get(i).getNumeroEtudiant() && i<App.ListeEtudiant.size()-1){
            i++;
        }
        if (!"".equals(modifNom.getText()))
            App.ListeEtudiant.get(i).setNom(modifNom.getText().toUpperCase());
        
        if (!"".equals(modifPrenom.getText()))
            App.ListeEtudiant.get(i).setPrenom(modifPrenom.getText().toUpperCase());
        
        if (!"".equals(modifMail.getText()))
            App.ListeEtudiant.get(i).setMail(modifMail.getText());
        
        g1.ecritureCSVetudiant("donnee_application/ListeEtudiant.csv", App.ListeEtudiant);
        alerteModif();
        Stage stage = (Stage) btValider.getScene().getWindow();
        stage.close();
        
        
    }
    
    /**
     * affiche une alerte quand l'etudiant est modifié
     */
    public void alerteModif(){
        Alert alertFormation = new Alert(Alert.AlertType.CONFIRMATION);
        alertFormation.setTitle("Etudiant modifié !");
        alertFormation.setHeaderText("L'étudiant a bien été modifié");
        alertFormation.showAndWait();
    }
    
    @FXML
    /**
     * changement de page vers l'inscription de l'etudiant aux UE du S1
     */
    public void ajoutUEs1(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("InscriptionUEEtudiant.fxml"));
            Parent root = loader.load();
            
            InscriptionUEEtudiantController iEUe = loader.getController();
            iEUe.recupererFormation(formation);
            iEUe.inscriptionEtudiants1(etudiant);
            
            Stage stage = (Stage) btInscriptionS1.getScene().getWindow();
            stage.setScene(new Scene(root,1290, 750));
            
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    
    }
     /**
      * changement de page vers l'inscription de l'etudiant aux UE du S2
      */
    public void ajoutUEs2(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("InscriptionUEEtudiant.fxml"));
            Parent root = loader.load();
            InscriptionUEEtudiantController iEUe = loader.getController();
            iEUe.recupererFormation(formation);
            iEUe.inscriptionEtudiants2(etudiant);

            
            Stage stage = (Stage) btInscriptionS2.getScene().getWindow();
            stage.setScene(new Scene(root,1290, 750));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    
    }
    /**
     * recupere la formation dans laquelle est inscrit l'etudiant
     * @param f 
     */
    public void recupererFormation(Formation f){
        this.formation =f;
      
    }
    
     
        
}
