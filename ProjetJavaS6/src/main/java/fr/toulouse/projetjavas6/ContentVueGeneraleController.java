/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.UniteEnseignement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML ContentVueGenerale
 */
public class ContentVueGeneraleController {

    @FXML
    private Label lbNomFormation;
    @FXML
    private TextField tfRecherche;
    @FXML
    private GridPane listEtudiantGrid;
    @FXML
    private ImageView btTrier;
    
    @FXML
    private AnchorPane anchorPaneVueGenerale;
    
    @FXML
    private VBox VboxVueGenerale;
    
    @FXML
    private Button btAjoutEtudiant;
    
    
    private Listener listener;
    
    private ListenerUE listenerUE;
    
    private Formation form;
    
    private String annee ;
   
    
    /**
     * Setter du nom de la formation
     * @param nomFormation
     * nom de la formation courante
     */
    public void setNomFormation(String nomFormation){
        lbNomFormation.setText(nomFormation); 
    }
    
    /**
     *affichage des card Etudiant pour une formation
     * @param formation
     * formation courante
     */
    public void affichageCardEtudiant(Formation formation){
        
        List<Etudiant> updateListeEtudiant = new ArrayList<>();
        
        String[] words = formation.getListeNumEtudiant().split("/");
        this.annee = formation.getAnneeDiplome();
        for (int j=0; j<words.length; j++) {
            
            for(int i=0; i<App.ListeEtudiant.size();i++){
                String numeroEtudiant = String.valueOf(App.ListeEtudiant.get(i).getNumeroEtudiant());
                if (words[j].equals(numeroEtudiant)) {
                    updateListeEtudiant.add(App.ListeEtudiant.get(i));
                }
            }       
        }
        
        //si la liste d'étudiant n'est pas vide
        if(updateListeEtudiant.size() > 0){
            
            //on fait appel au listener et on définit l'action à réaliser après un click 
            //(definition ici de la méthode dans l'interface)
            listener = new Listener() {
                @Override
                public void onClickListener(Etudiant etudiant, Formation Formation, AnchorPane anchorPane) {
                    try{
                        
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("ProfilEtudiant.fxml"));
                        Parent root = loader.load();
                        ProfilEtudiantController profilEtudiantController = loader.getController();
                        
                        profilEtudiantController.initialiserProfil(etudiant);
                        profilEtudiantController.creationTabAnnee(etudiant.getNiveau(), formation);
                        profilEtudiantController.setProgressBarAnnee(formation);
                        
                        Stage newStage = new Stage();
                        newStage.setScene(new Scene(root,1290, 750));
                        newStage.show();
                        
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } 
                }
            };
        }
        
        int column = 0;
        int row = 1;
        
        try {
            for(int i=0; i<updateListeEtudiant.size();i++){
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("CardEtudiant.fxml"));
                
                AnchorPane cardEtudiant = fxmlLoader.load();
                CardEtudiantController cardEtudiantController = fxmlLoader.getController();
                cardEtudiantController.setDataCardEtudiant(updateListeEtudiant.get(i), listener);
                
                
                if(column == 6){
                    column = 0;
                    row++;
                }
                
                listEtudiantGrid.add(cardEtudiant, column++, row);
                GridPane.setMargin(cardEtudiant, new Insets(10));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Affichage des card UE pour une formation
     * @param formation
     * formation courante
     */
    public void affichageCardUE(Formation formation){
        
        List<UniteEnseignement> updateListeUE = new ArrayList<>();
        
        String[] words = formation.getListeUE().split("/");
        
        for (int j=0; j<words.length; j++) {
            for (int i=0; i < App.ListeUE.size(); i++) {
                String codeUE = App.ListeUE.get(i).getCodeUE();
            
                if (words[j].equals(codeUE)) {
                    updateListeUE.add(App.ListeUE.get(i));
                }
            }
        }
        
        //si la liste d'UE n'est pas vide
        if(updateListeUE.size() > 0){
            
            //on fait appel au listener et on définit l'action à réaliser après un click 
            //(definition ici de la méthode dans l'interface)
            this.listenerUE = new ListenerUE() {
                @Override
                public void onClickListener(UniteEnseignement UE, Formation formation) {
                    try{
                        
                        FXMLLoader loader = new FXMLLoader();
                        loader.setLocation(getClass().getResource("ProfilUE.fxml"));
                        
                        Parent root = (Parent) loader.load();
                        ProfilUEController profilUEController = loader.getController();
                        profilUEController.initialiserProfilUE(UE, formation);
                        profilUEController.sendFormation(formation);
                        profilUEController.affichageCardEtudiant(UE);
                        
                        Stage newStage = new Stage();
                        newStage.setScene(new Scene(root,1290, 750));
                        newStage.show();
                        
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            };
        }
        
        int column = 0;
        int row = 1;
        
        try {
            for(int i=0; i<updateListeUE.size();i++){
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("CardUE.fxml"));
                
                AnchorPane cardUE = fxmlLoader.load();
                CardUEController cardUEController= fxmlLoader.getController();
                cardUEController.setDataCardUE(updateListeUE.get(i), listenerUE);
                cardUEController.sendData(formation);
                
                if(column == 6){
                    column = 0;
                    row++;
                }
                
                listEtudiantGrid.add(cardUE, column++, row);
                GridPane.setMargin(cardUE, new Insets(10));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Gère le clic sur le bouton ajout etudiant
     */
    @FXML
    public void ajoutEtudiant(){
        try {
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("AjoutEtudiant.fxml"));
            Parent root = loader.load();

            
            Stage stage = (Stage) btAjoutEtudiant.getScene().getWindow();
            stage.setScene(new Scene(root,1290, 750));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    
}
