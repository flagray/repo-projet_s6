/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Formation;
import java.util.List;
import javafx.scene.layout.VBox;

/**
 * Ecouteur pour savoir si une formation est cliqué par l'utilisateur
 */
public interface ListenerAccueil {

    /**
     * permet de gérer le click sur les formation au niveau de la page accueil
     * @param listeFormation
     * liste des formation
     * @param vbox
     * element FXML contenant l'element parent de la page Accueil
     */
    public void onClickListener(List<Formation> listeFormation, VBox vbox);
    
    
}
