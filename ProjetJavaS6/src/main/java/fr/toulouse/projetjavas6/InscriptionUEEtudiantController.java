/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import fr.toulouse.projetjavas6.Backend.GestionCSV;
import fr.toulouse.projetjavas6.Backend.UniteEnseignement;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML InscriptionUEEtudiant
 */
public class InscriptionUEEtudiantController {
    private Formation formation;
    private Etudiant etudiant;
    private GestionCSV csv = new GestionCSV();
    private boolean verifCreneau = true;
    @FXML
    private VBox backVbox;
    @FXML
    private ListView <CheckBox> ListViewUE;
    @FXML
    private Button button;
    List<UniteEnseignement> listeUEformation = new ArrayList<>();
    List<UniteEnseignement> listeUEnonInscrites = new ArrayList<>();
    List<UniteEnseignement> listeUEinscrites = new ArrayList<>();
 
    
    /**
     * recupere les UE du S1 de la formation de l'etudiant et les UE auxquelles il n'est pas inscrit pour les afficher dans une liste de checkbox
     * @param e etudiant à inscrire aux UE
     * 
     * 
     */
    public void inscriptionEtudiants1(Etudiant e){
        this.etudiant =e;

        
      
        
        String [] numeroUEFormation = this.formation.getListeUE().split("/");
        
        //on parcourt la liste des UE inscrites à la formation
        for(int i=0;i<numeroUEFormation.length;i++){
            
            //on parcourt la liste complete des UE
            for (int j=0;j<App.ListeUE.size();j++){
                //on vérifie que l'UE de la formation et l'UE de la liste d'ue sont les mêmes et si le semestre est le semstre 1
                if(App.ListeUE.get(j).getCodeUE().equals(numeroUEFormation[i]) && App.ListeUE.get(j).getSemestre().equals("1")){
                    
                   
                    listeUEformation.add(App.ListeUE.get(j));
                    listeUEnonInscrites.add(App.ListeUE.get(j));
                    String [] numeroEtudiantUE = App.ListeUE.get(j).getListeNumEtudiant().split("/");
                    
                    //on parcourt la liste des etudiants inscrits à l'UE pour chaque UE de la formation
                    for (int k=0;k<numeroEtudiantUE.length;k++)
                    {
                        // on vérifie si le l'etudiant est inscrit à l'UE 
                       if ((e.getNumeroEtudiant()+"").equals(numeroEtudiantUE[k])) {
                           listeUEinscrites.add(App.ListeUE.get(j));
                           listeUEnonInscrites.remove(App.ListeUE.get(j));
                       }
                    }
                }
            
            }
        }
        

        
    
    
    
        ListViewUE.setStyle("-fx-font-family:Calibri Light; -fx-control-inner-background: #F5F5F5; -fx-control-inner-background-alt: derive(-fx-control-inner-background, 50%);");

        
        
       
        
      for (int i = 0; i < listeUEnonInscrites.size(); i++) {
            String nomUECreneau  = listeUEnonInscrites.get(i).getNomUE()+" -- Creneau : " + listeUEnonInscrites.get(i).getCreneau();
            CheckBox check = new CheckBox(nomUECreneau);
            ListViewUE.getItems().add(check);
            
        }
        
        
     

    }
    
    /**
     * recupere les UE du S2 de la formation de l'etudiant et les UE auxquelles il n'est pas inscrit pour les afficher dans une liste de checkbox
     * @param e etudiant à inscrire aux UE
     */
    public void inscriptionEtudiants2(Etudiant e){
        this.etudiant =e;

      
        
       
        
        String [] numeroUEFormation = this.formation.getListeUE().split("/");
        
        for(int i=0;i<numeroUEFormation.length;i++){
            
         
            for (int j=0;j<App.ListeUE.size();j++){
                
                if(App.ListeUE.get(j).getCodeUE().equals(numeroUEFormation[i]) && App.ListeUE.get(j).getSemestre().equals("2")){
                    
                   
                    listeUEformation.add(App.ListeUE.get(j));
                    listeUEnonInscrites.add(App.ListeUE.get(j));
                    String [] numeroEtudiantUE = App.ListeUE.get(j).getListeNumEtudiant().split("/");
                    
                    for (int k=0;k<numeroEtudiantUE.length;k++)
                    {
                       if ((e.getNumeroEtudiant()+"").equals(numeroEtudiantUE[k])) {
                           listeUEinscrites.add(App.ListeUE.get(j));
                           listeUEnonInscrites.remove(App.ListeUE.get(j));
                       }
                    }
                }
            
            }
        }
        

        
    
    
    
        ListViewUE.setStyle("-fx-font-family:Calibri Light; -fx-control-inner-background: #F5F5F5; -fx-control-inner-background-alt: derive(-fx-control-inner-background, 50%);");

        
        
       
        
      for (int i = 0; i < listeUEnonInscrites.size(); i++) {
            String nomUECreneau  = listeUEnonInscrites.get(i).getNomUE()+" -- Creneau : " + listeUEnonInscrites.get(i).getCreneau();
            CheckBox check = new CheckBox(nomUECreneau);
            ListViewUE.getItems().add(check);
            
        }
        
        
     

    }
    
  
      
      
    

    

    /**
     * recupere la formation dans laquelle l'etudiant est inscrite
     * @param f formation à recuperer
     */
    public void recupererFormation (Formation f){
        this.formation = f;
    }
    
    
    @FXML
    /**
     * ajoute l'étudiant à toutes les UE auxquelles il souhaite si'inscrire lorsqu'il clique sur le bouton valider.
     */
    public void handleValider(){
        String ueErreur="";
        String erreur;
        
        String ajoutEtudiant =etudiant.getNumeroEtudiant()+"/";
        
        Stage stage = (Stage) button.getScene().getWindow();
        int i=0;
        for (CheckBox box : ListViewUE.getItems()){
            if (box.isSelected()) {
                for (int j=0; j<App.ListeUE.size();j++){
                   if(App.ListeUE.get(j)==listeUEnonInscrites.get(i)){
                       ueErreur=verificationCreneau(listeUEnonInscrites.get(i),ueErreur);
                       if(this.verifCreneau){
                            
                            if ("null".equals(App.ListeUE.get(j).getListeNumEtudiant())){
                                 App.ListeUE.get(j).setListeNumEtudiant(ajoutEtudiant);
                            }
                            else{

                                App.ListeUE.get(j).setListeNumEtudiant(App.ListeUE.get(j).getListeNumEtudiant()+ajoutEtudiant);
                                
                            }
                            Alert alertValidation = new Alert(Alert.AlertType.INFORMATION);
                            alertValidation.setTitle("UE ajoutée");
                            alertValidation.setHeaderText("L'inscription à l'UE " + App.ListeUE.get(j).getNomUE() + " a bien été effectuée !");
                            alertValidation.showAndWait();
                            listeUEinscrites.add(App.ListeUE.get(j));
                       }
                       else{
                           erreur="Vous ne pouvez pas sélectionner l'UE "+ App.ListeUE.get(j).getNomUE() + " car le créneau n'est pas disponible";
                           Alert alertCreneau = new Alert(Alert.AlertType.ERROR);
                           alertCreneau.setTitle("Problème de creneau");
                           alertCreneau.setHeaderText("L'étudiant est déja inscrit à l'UE " + ueErreur + " sur le même créneau.");
                           alertCreneau.setContentText(erreur);
                           alertCreneau.showAndWait();
                           this.verifCreneau=true;
                           stage.close();
                       }
                    }
                }
            } 
            i++;
            
        }
        csv.ecritureCSVUE("donnee_application/UE.csv", App.ListeUE);
        stage.close();
        
        
        
        

    }

    /**
     * on verifie si l'ue ajoutée est sur le même creneau qu'une ue auxuquelle l'etudiant est deja inscrit.
     * @param UE UniteEnseignement 
     * @param erreur String
     * @return erreur String nom de l'UE deja inscrite sur le creneau.
    */
    public String verificationCreneau(UniteEnseignement UE, String erreur){
        
       
        for (int i=0; i<listeUEinscrites.size();i++){
            if(UE.getCreneau()==listeUEinscrites.get(i).getCreneau()){
               
                erreur=listeUEinscrites.get(i).getNomUE();
                this.verifCreneau=false;
            }
        }
        
        return erreur;
    }
    
}
   
    
  
