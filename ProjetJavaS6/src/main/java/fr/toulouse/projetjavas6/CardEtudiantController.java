package fr.toulouse.projetjavas6;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

import fr.toulouse.projetjavas6.Backend.Etudiant;
import fr.toulouse.projetjavas6.Backend.Formation;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;

/**
 * Controller du fichier FXML CardEtudiant
 */
public class CardEtudiantController{

    @FXML
    private Label lbPrenom;
    @FXML
    private Label lbNom;
    @FXML
    private Circle pPhoto;
    @FXML
    private Label lbNumEtudiant;

    
    private Etudiant etudiant;
    
    private Listener listener;
    
    private Formation formation;
    
    private AnchorPane anchorPane = null;
    

    /**
     * gère le click sur une card etudiant
     * @param mouseEvent 
     */
    @FXML
    private void click(MouseEvent mouseEvent){
        listener.onClickListener(etudiant, formation, anchorPane);
    }
    
    /**
     * gère les données affichées sur les card etudiant
     * @param etudiant
     * Etudiant courant
     * @param listener1
     * Permet de mettre a jour le Listener1
     */
    public void setDataCardEtudiant(Etudiant etudiant, Listener listener1){
        this.etudiant = etudiant;
        this.listener = listener1;
        
        lbPrenom.setText(etudiant.getPrenom());
        lbNom.setText(etudiant.getNom());
        Integer numEtu = etudiant.getNumeroEtudiant();
        this.lbNumEtudiant.setText(numEtu.toString());
    }
    /**
     * Permet de transmetre la formation courante et la fenetre FXML globale pour le clic sur la card
     * @see click(MouseEvent)
     * @param anchorPane
     * Element de la fenetre parent
     * @param formation 
     * Formation courante
     */
    public void sendData(AnchorPane anchorPane, Formation formation){
        this.anchorPane = anchorPane;
        this.formation = formation;
    }
    
}
