/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Formation;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * Controller du fichier FXML ContainerFormationAccueil
 */
public class ContainerFormationAccueilController{

    @FXML
    private Label lbNomFormation;
    @FXML
    private VBox backVbox;
    
    private VBox vbox;
 
    
    /**
     * Setter du nom de la formation
     * @param nomDiplome
     * nom de la formation courante
     */
    public void setNomFormation(String nomDiplome){
        this.lbNomFormation.setText(nomDiplome);
    }
    
    /**
     * Affichage des card Specialite au niveau de la page Accueil, permet de gérer le clic sur les élements contenu dans les card specialité
     */
    public void affichageCardSpecialite(){
        //permet de gérer l'affichage sur l'interface graphique
        List<String> listeNomSpecialite = new ArrayList<>();
        //permettra de mettre à jour la liste de formation pour un diplome précis
        List<Formation> updateListeFormation = new ArrayList<>();
        
        //on parcours la listeFormation -> si le nom du diplome (sur l'interface graphique) est égal au nom du diplome de listeFormation de i
        //Alors on ajoute à notre updateListFormation listeFormation de i
        for (Formation f : App.ListeFormation) {
            if (f.getDiplome().getNomDiplome().equals(lbNomFormation.getText())) {
                updateListeFormation.add(f);
            }
        }
        
        //on parcours la liste mis à jour -> si notre listeNomSpecialité ne contient pas la spécialité alors on l'ajoute
        for (Formation f : updateListeFormation) {
            if (!listeNomSpecialite.contains(f.getMention().getSpecialite())) {
                listeNomSpecialite.add(f.getMention().getSpecialite());
            }
        }

        
        //affichage graphique
        ScrollPane scrollPane = new ScrollPane();
        backVbox.getChildren().add(scrollPane);
        
        //Style ScrollPane
        scrollPane.setStyle("-fx-background-color:white;");
        scrollPane.setPrefSize(1290,750);
        
        GridPane backGridPane = new GridPane();
        scrollPane.setContent(backGridPane);
        
        int column = 0;
        int row = 1;

        try {
            for(int i=0; i<listeNomSpecialite.size();i++){
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("CardSpecialite.fxml"));
                VBox cardSpecialite = fxmlLoader.load();
                
                //Pour gérer l'interieur des cardSpecialité on recupere son cotroller
                CardSpecialiteController cardSpecialiteController = fxmlLoader.getController();
                //et ses méthodes
                cardSpecialiteController.passerElem(this.vbox);//permet de vider la fenetre quand on clic
                cardSpecialiteController.setNomSpecialite(listeNomSpecialite.get(i));
                cardSpecialiteController.affichageContainerDiplome(updateListeFormation);
                
                
                backGridPane.add(cardSpecialite, column++, row);
                GridPane.setMargin(cardSpecialite, new Insets(10));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    

    /**
     * parmet de passer l'element FXML de la page d'accueil au fichier FXML suivant
     * @param vbox
     * element FXMM parent général de la page accueil
     */
    public void passerElem(VBox vbox){ 
        this.vbox = vbox;
    }
    
}
