
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package fr.toulouse.projetjavas6;

import fr.toulouse.projetjavas6.Backend.Formation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Controller du fichier FXML CardSpecialite
 */
public class CardSpecialiteController{


    @FXML
    private Label lbNomSpecialite;
    @FXML
    private VBox backVbox;
    
    private ListenerAccueil listener;
 
    private VBox vbox;
    
    /**
     * Setter du label NomSpecialite
     * @param nomSpecialite 
     * nom de la specialité
     */
    public void setNomSpecialite(String nomSpecialite){
        lbNomSpecialite.setText(nomSpecialite);
    }
    
    /**
     * Affichage du container permettant d'afficher les diplomes, puis gère le clic sur ce diplome
     * @param listeFormation
     * Liste de la formation courante
     */
    public void affichageContainerDiplome(List<Formation> listeFormation){
        
        //permet de gérer l'affichage sur l'interface graphique
        List<String> listeNomMention = new ArrayList<>();
        List<String> listeNomParcours = new ArrayList<>();
        //permettra de mettre à jour la liste de formation pour une spécialité précise
        List<Formation> updateListeFormation = new ArrayList<>();
        
        
        for (Formation f : listeFormation) {
            if (f.getMention().getSpecialite().equals(lbNomSpecialite.getText())) {
                updateListeFormation.add(f);
            }
        }

        for (Formation f : updateListeFormation) {
            if (!listeNomParcours.contains(f.getMention().getParcours())) {
                listeNomParcours.add(f.getMention().getParcours());
                listeNomMention.add(f.getMention().getNomMention());
            }
        }

        //permet de gérer le clic sur une mention
        if (updateListeFormation.size()>0) {
            this.listener = new ListenerAccueil() {
                @Override
                public void onClickListener(List<Formation> listeFormation, VBox vbox) {
                        //quand on clic sur une mention on vide la scene courante et on y applique une nouvelle scene avec VueGeneraleTabPane
                        try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("VueGeneraleTabPane.fxml"));
                            Parent root = loader.load();
                            
                            VueGeneraleTabPaneController vueGeneraleTabPaneController = loader.getController();
                            vueGeneraleTabPaneController.sendData(listeFormation);
                            vueGeneraleTabPaneController.initialisationVueGenerale(listeFormation);
                            
                            
                            Stage stage = (Stage) vbox.getScene().getWindow();
                            stage.setScene(new Scene(root,1290, 750));
                            
                        } catch (IOException ex) {
                        ex.printStackTrace();
                        }
                        
                        
                    }
            };
        }
        
        //gère l'affichage dynamique des mention dans les cardSpecialite
        ScrollPane scrollPane = new ScrollPane();
        backVbox.getChildren().add(scrollPane);
        
        GridPane backGridPane = new GridPane();
        scrollPane.setContent(backGridPane);
        
        int column = 0;
        int row = 1;

        try {
            for(int i=0; i<listeNomParcours.size();i++){
                
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("ContainerDiplome.fxml"));
                AnchorPane containerDiplome = fxmlLoader.load();
                
                ContainerDiplomeController containerDiplomeController = fxmlLoader.getController();
                containerDiplomeController.passerElem(this.vbox);//permet de vider la fenetre quand on clic
                containerDiplomeController.setNomMention(listeNomMention.get(i));
                containerDiplomeController.setNomParcours(listeNomParcours.get(i));
                containerDiplomeController.intialiserContainerDiplome(updateListeFormation, this.listener);
                
                
                if(column == 1){
                    column = 0;
                    row++;
                }

                backGridPane.add(containerDiplome, column++, row);
                //GridPane.setMargin(containerDiplome, new Insets(10));
                

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    

    /**
     * parmet de passer l'element FXML de la page d'accueil au fichier FXML suivant
     * @param vbox
     * element FXMM parent général de la page accueil
     */
    public void passerElem(VBox vbox){
        this.vbox = vbox;
    }
    
}
