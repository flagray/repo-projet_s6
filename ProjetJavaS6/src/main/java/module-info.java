module fr.toulouse.projetjavas6 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;
    

    opens fr.toulouse.projetjavas6 to javafx.fxml;
    exports fr.toulouse.projetjavas6;
    exports fr.toulouse.projetjavas6.Backend;
}
